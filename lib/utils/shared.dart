import 'package:shared_preferences/shared_preferences.dart';

class SharedPref {
  static const String KEY = "NAMA";

  static Future<String> getNamaFromPreferences() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(KEY) ?? null;
  }
 
  static Future<bool> saveNamaToPreferences(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(KEY, value);
  }
}