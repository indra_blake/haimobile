import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final _databaseName = 'haimbile.db';
  static final _dbVersion = 3; //2 next upload

  static final table = 'user';

  static final columnId = 'id';
  static final columnImagePath = 'imagepath';
  static final columnNama = 'nama';
  static final columnUnit = 'unit';
  static final columnNopung = 'nopung';
  // static final columnSchedulePosition = 'scheduleposition';
  // static final columnTimerWallPosition = 'timerposition';
  // static final columnSetOfftime = 'offtime';
  // static final columnSun = 'sun';
  // static final columnMon = 'mon';
  // static final columnTue = 'tue';
  // static final columnWed = 'wed';
  // static final columnThu = 'thu';
  // static final columnFri = 'fri';
  // static final columnSat = 'sat';
  // static final columnEnable = 'enabled';
  // static final columnCountDownTimeSetup = 'timersetup';
  // static final columnCountDownTimer = 'timer';
  // static final columnCountOnOrOff = 'status';
  // static final columnTimeTitle = 'timetitle';
  // static final columnCountdownEnable = 'countdownenabled';
  String path;

  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    path = join(documentDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _dbVersion, onCreate: _onCreate, onUpgrade: _onUpgrade);
  }

  void _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      // await db.execute("ALTER TABLE $table ADD COLUMN $columnWallPosition TEXT NOT NULL");
      // await db.execute("ALTER TABLE $table2 ADD COLUMN $columnTimerWallPosition TEXT NOT NULL");
      //_onCreate(db, _dbVersion);
      //print('Versi 2 database');
      await deleteDatabase(path);
      _onCreate(db, _dbVersion);
      print('Success altertable database');
    }
  }

  /* create database */
  Future _onCreate(Database db, int version) async {
    //await db.execute('DROP TABLE IF EXISTS $table');
    await db.execute('''
      CREATE TABLE $table(
        $columnId INTEGER PRIMARY KEY,
        $columnImagePath TEXT NULL,
        $columnNama TEXT NULL,
        $columnUnit TEXT NULL,
        $columnNopung TEXT NULL ) 
        ''');
    //await db.execute('DROP TABLE IF EXISTS $table2');
    // await db.execute('''
    //   CREATE TABLE $table(
    //     $columnId INTEGER PRIMARY KEY,
    //     $columnImagePath TEXT NOT NULL,
    //     $columnNama TEXT NOT NULL,
    //     $columnUnit INTEGER NOT NULL,
    //      )
    //     ''');
  }

  /* insert method */
  Future<int> insert(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(table, row);
  }

  Future<int> insertUser(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(table, row);
  }

  /* get all rows */
  Future<List<Map<String, dynamic>>> queryAllRows() async {
    Database db = await instance.database;
    return await db.query(table);
  }

  // Future<List<Map<String, dynamic>>> queryAllRowsCountdown() async {
  //   Database db = await instance.database;
  //   return await db.query(table2);
  // }

  /* get single row */
  Future<List<Map<String, dynamic>>> getAvatarPath(path) async {
    Database db = await instance.database;
    return await db.rawQuery('SELECT * FROM user WHERE imagepath = ?', [path]);
  }

  Future<List<Map<String, dynamic>>> updateUser(nama, unit, nopung, image) async {
    Database db = await instance.database;
    return await db.rawQuery(
        'UPDATE user SET nama = ?, unit = ?, nopung = ?, imagepath = ?', [nama, unit, nopung, image]);
  }

  // Future<List<Map<String, dynamic>>> getSingle2(serial,ids) async {
  //   Database db = await instance.database;
  //   print('Data ID dr Schedule: $ids');
  //   return await db.rawQuery('SELECT * FROM timer WHERE timername = ? AND id = $ids', [serial]);
  // }

  // Future<List<Map<String, dynamic>>> getSingleCountdown(serial) async {
  //   Database db = await instance.database;
  //   return await db.rawQuery('SELECT * FROM countdown WHERE timername = ?', [serial]);
  // }

  // Future<List<Map<String, dynamic>>> getSingleCountdown2(serial,ids) async {
  //   print('Data ID dr countdown: $ids');
  //   Database db = await instance.database;
  //   return await db.rawQuery('SELECT * FROM countdown WHERE timername = ? AND id = $ids', [serial]);
  // }

  // /* query table count  */
  // Future<int> queryRowcount() async {
  //   Database db = await instance.database;
  //   return Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT (*) FROM $table'));
  // }

  /* update */
  Future<int> update(Map<String, dynamic> row) async {
    Database db = await instance.database;
    int id = row[columnId];
    return await db.update(table, row, where: '$columnId = ?', whereArgs: [id]);
  }

  /* update */
  // Future<int> updateCountDown(Map<String, dynamic> row) async {
  //   Database db = await instance.database;
  //   String serial = row[columnTimerName];
  //   return await db.update(table2, row,
  //     where: '$columnTimerName = ?', whereArgs: [serial]
  //   );
  // }

  /* Delete */
  // Future<int> delete(String path) async {
  //   Database db = await instance.database;
  //   return await db.delete(table,where: '$columnTimerName = ?', whereArgs: [serial]);
  // }

  // Future<int> delete2(int id, String serial) async {
  //   print('Data ID dr Schedule: $id');
  //   Database db = await instance.database;
  //   return await db.delete(table, where: '$columnId = $id AND $columnTimerName = ?', whereArgs: [serial]);
  // }

  // Future<int> deleteCount(String serial) async {
  //   Database db = await instance.database;
  //   return await db.delete(table2,where: '$columnTimerName = ?', whereArgs: [serial]);
  // }

  // Future<int> deleteCount2(int id, String serial) async {
  //   print('Data ID dr Countdown: $id');
  //   Database db = await instance.database;
  //   return await db.delete(table2,where: '$columnId = $id AND $columnTimerName = ?', whereArgs: [serial]);
  // }
}
