import 'package:easy_loader/easy_loader.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:mobile/login.dart';
import 'package:mobile/utils/fadeanimation.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:toast/toast.dart';

class DaftarPage extends StatefulWidget {
  @override
  _DaftarPageState createState() => _DaftarPageState();
}

class _DaftarPageState extends State<DaftarPage> {
  final _firebaseAuth = FirebaseAuth.instance;
  TextEditingController emailControllerRegister = TextEditingController();
  TextEditingController passwordControllerRegister = TextEditingController();
  bool isLogin = false;
  bool _isHidePassword = true;
  bool isLoginrun = false;
  bool emailNotValid = false;

  /* Function for signup */
  Future<void> signUpEmailPassword(String email, String password) async {
    //context.loaderOverlay.show();
    print('jalanin register dulu');
    User user;
    try {
      UserCredential userCredential = await _firebaseAuth
          .createUserWithEmailAndPassword(email: email, password: password);
      user = userCredential.user;
      user.sendEmailVerification();

      print('Credential: $userCredential');
      //context.loaderOverlay.hide();
      emailControllerRegister.clear();
      passwordControllerRegister.clear();
      Future.delayed(Duration(seconds: 2), () {
        setState(() {
          isLoginrun = false;
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => Login()),
            (Route<dynamic> route) => false,
          );
        });
      });

      //signInEmailAndPassword(email, password);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'wrong-password') {
        Toast.show('Password Salah', context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
        //runChangeButtonRelay();
        //context.loaderOverlay.hide();
      } else if (e.code == "invalid-email") {
        setState(() {
          emailNotValid = true;
        });
        // Toast.show('Alamat email tidak valid', context,
        //     duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      } else if (e.code == "user-not-found") {
        //runChangeButtonRelay();
        //context.loaderOverlay.hide();
        Toast.show('User tidak ditemukan', context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
        const backTo = Duration(seconds: 2);
        //Timer(backTo, () => showAlertToRegister(context));
      } else if (e.code == 'too-many-requests') {
        //runChangeButtonRelay();
        //context.loaderOverlay.hide();
        Toast.show('Anda sudah mencoba berkali-kali, coba lagi nanti', context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      }
      print(e);
    } catch (e) {
      print(e);
    }
  }

  Future<void> sendEmailVerification() async {
    final User user = _firebaseAuth.currentUser;
    user.sendEmailVerification();
  }

  void _toggle() {
    setState(() {
      _isHidePassword = !_isHidePassword;
    });
  }

  _onInfo(context) {
    Alert(
      context: context,
      type: AlertType.warning,
      title: "Informasi",
      desc: "Silahkan Klik link Verifikasi yang dikirim ke Email anda!",
      buttons: [
        DialogButton(
          child: Text(
            "TUTUP",
            style: TextStyle(color: Colors.white, fontSize: 17),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          width: 120,
        )
      ],
      alertAnimation: fadeAlertAnimation2,
    ).show();
  }

  Widget fadeAlertAnimation2(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    return Align(
      child: FadeTransition(
        opacity: animation,
        child: child,
      ),
    );
  }

  @override
  void dispose() {
    emailControllerRegister.dispose();
    passwordControllerRegister.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Container(
              child: Column(
                children: <Widget>[
                  Container(
                    height: 400,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/bg.png'),
                            fit: BoxFit.fill)),
                    child: Stack(
                      children: <Widget>[
                        // Positioned(
                        //   left: 30,
                        //   width: 80,
                        //   height: 200,
                        //   child: FadeAnimation(1, Container(
                        //     decoration: BoxDecoration(
                        //       image: DecorationImage(
                        //         image: AssetImage('assets/logosplash.png')
                        //       )
                        //     ),
                        //   )),
                        // ),
                        Positioned(
                          left: 140,
                          width: 80,
                          height: 150,
                          child: FadeAnimation(
                              1.3,
                              Container(
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage(
                                            'assets/logosplash.png'))),
                              )),
                        ),
                        // Positioned(
                        //   right: 40,
                        //   top: 40,
                        //   width: 80,
                        //   height: 150,
                        //   child: FadeAnimation(1.5, Container(
                        //     decoration: BoxDecoration(
                        //       image: DecorationImage(
                        //         image: AssetImage('assets/logosplash.png')
                        //       )
                        //     ),
                        //   )),
                        // ),
                        Positioned(
                          child: FadeAnimation(
                              1.6,
                              Container(
                                margin: EdgeInsets.only(bottom: 30),
                                child: Center(
                                  child: Text(
                                    "Daftar",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 30,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              )),
                        ),
                        Positioned(
                          child: FadeAnimation(
                              1.6,
                              Container(
                                margin: EdgeInsets.only(top: 60),
                                child: Center(
                                  child: Text(
                                    "HAIMobile",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 40,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              )),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(30.0),
                    child: Column(
                      children: <Widget>[
                        FadeAnimation(
                            1.8,
                            Container(
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Color.fromRGBO(0, 11, 31, 183),
                                        blurRadius: 20.0,
                                        offset: Offset(0, 10))
                                  ]),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.all(8.0),
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom: BorderSide(
                                                color: Colors.grey[100]))),
                                    child: TextField(
                                      controller: emailControllerRegister,
                                      keyboardType: TextInputType.emailAddress,
                                      decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: "Masukan Email",
                                          hintStyle: TextStyle(
                                              color: Colors.grey[400])),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(8.0),
                                    child: TextField(
                                      controller: passwordControllerRegister,
                                      obscureText: _isHidePassword,
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: "Kata Sandi",
                                        hintStyle:
                                            TextStyle(color: Colors.grey[400]),
                                        suffixIcon: GestureDetector(
                                          onTap: () {
                                            _toggle();
                                          },
                                          child: Icon(
                                            _isHidePassword
                                                ? Icons.visibility_off
                                                : Icons.visibility,
                                            color: _isHidePassword
                                                ? Colors.grey
                                                : Colors.blue,
                                            size: 20.0,
                                          ),
                                        ),
                                        isDense: true,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )),
                        SizedBox(
                          height: 30,
                        ),
                        FadeAnimation(
                            2,
                            GestureDetector(
                                onTap: () {
                                  if (emailControllerRegister.text.isEmpty) {
                                    Toast.show(
                                        'Email tidak boleh kosong', context,
                                        duration: Toast.LENGTH_LONG,
                                        backgroundColor: Colors.black);
                                    // SmartDialog.showToast(
                                    //     'Email tidak boleh kosong');
                                  } else if (passwordControllerRegister
                                      .text.isEmpty) {
                                    Toast.show(
                                        'Password tidak boleh kosong', context,
                                        duration: Toast.LENGTH_LONG,
                                        backgroundColor: Colors.black);
                                    // SmartDialog.showToast(
                                    //     'Password tidak boleh kosong');
                                  } else if (passwordControllerRegister
                                          .text.length <
                                      5) {
                                    Toast.show(
                                        'Password minimal 8 digit', context,
                                        duration: Toast.LENGTH_LONG,
                                        backgroundColor: Colors.black);
                                    // SmartDialog.showToast(
                                    //     'Password minimal 8 digit');
                                  } else if (emailControllerRegister
                                          .text.length <
                                      8) {
                                    Toast.show('Email tidak valid', context,
                                        duration: Toast.LENGTH_LONG,
                                        backgroundColor: Colors.black);
                                    // SmartDialog.showToast('Email tidak valid');
                                  } else {
                                    _showLoading();
                                    signUpEmailPassword(
                                        emailControllerRegister.text,
                                        passwordControllerRegister.text);
                                    setState(() {
                                      isLoginrun = true;
                                    });
                                  }
                                  print('Daftar');
                                },
                                child: Container(
                                  height: 50,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      gradient: LinearGradient(colors: [
                                        Colors.blue[800],
                                        Colors.blueAccent[700],
                                      ])),
                                  child: Center(
                                    child: Text(
                                      "Buat Akun",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ))),
                        SizedBox(
                          height: 10,
                        ),
                        FadeAnimation(
                            1.5,
                            Text(
                              "Sudah punya akun?",
                              style: TextStyle(color: Colors.blue[800]),
                            )),
                        FadeAnimation(
                            1.5,
                            TextButton(
                                onPressed: () {
                                  Navigator.of(context).pushAndRemoveUntil(
                                      MaterialPageRoute(
                                          builder: (context) => Login()),
                                      (Route<dynamic> route) => false);
                                },
                                child: Text(
                                  "Masuk!",
                                  style: TextStyle(color: Colors.blue[800]),
                                ))),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          isLoginrun
              ? EasyLoader(
                  image: AssetImage(
                    'assets/logosplash.png',
                  ),
                  iconColor: Colors.white,
                )
              : Container()
        ],
      ),
    );
  }

  void _showLoading() async {}
}
