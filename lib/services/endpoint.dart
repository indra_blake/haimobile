class Endpoint {
  static const BASE_URL =
      'https://hyundaiaccentindonesia.com/mobile/public/api';

  /* endpoint member */
  static const ALL_MEMBER = '/member';
  static const ALL_MEMBER2 = '/member2';
  static const MEMBER_SINGLE = '/member/';
  static const ADD_MEMBER = '/addmember';
  static const EDIT_MEMBER = '/editmember/';
  static const EDIT_MEMBER_NO_IMAGE = '/editmember2/';
  static const DELETE_MEMBER = '/delmember/{id}';

  /* endpoint event */
  static const ALL_EVENT = '/events';
  static const ADD_EVENT = '/addevent';
  static const EDIT_EVENT = '/editevent/{id}';
  static const DELETE_EVENT = '/delevent/{id}';

  /* endpoint  Pengurus*/
  static const ALL_PENGURUS = '/pengurus';
  static const ADD_PENGURUS = '/addpengurus';
  static const EDIT_PENGURUS = '/editpengurus/{id}';
  static const DELETE_PENGURUS = '/delpengurus/{id}';

  /* endpoint  Story*/
  static const ALL_STORIES = '/stories';
  static const ADD_STORIES = '/addstories';
}
