import 'dart:async';
import 'dart:io';

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:easy_loader/easy_loader.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:images_picker/images_picker.dart';
import 'package:intl/intl.dart';
import 'package:mobile/services/endpoint.dart';
import 'package:mobile/utils/shared.dart';
import 'dart:math' as math;
import 'package:mobile/views/dashboard.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;

class Daftar extends StatefulWidget {
  @override
  _DaftarState createState() => _DaftarState();
}

class _DaftarState extends State<Daftar> {
  bool setuju = false;
  bool isLoginrun = false;
  bool isInternetActive = false;
  bool isRegisActive = false;
  bool isDaftar = false;
  final Connectivity _connectivity = Connectivity();
  String imagepath;
  String appDocPath;
  String fileName;
  var fileGambar;
  bool isValid = false;
  SharedPreferences prefs;
  TextEditingController _nama = TextEditingController();
  TextEditingController _telp = TextEditingController();
  TextEditingController _domisili = TextEditingController();
  TextEditingController _alamat = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _tipe_mobil = TextEditingController();
  TextEditingController _tahun = TextEditingController();
  TextEditingController _no_sim = TextEditingController();
  TextEditingController _no_rangka = TextEditingController();
  TextEditingController _status = TextEditingController();

  final _namaKey = GlobalKey<FormState>();
  final _telpKey = GlobalKey<FormState>();
  final _domiKey = GlobalKey<FormState>();
  final _almtKey = GlobalKey<FormState>();
  final _emailKey = GlobalKey<FormState>();
  final _unitKey = GlobalKey<FormState>();
  final _tahunKey = GlobalKey<FormState>();
  final _nosimKey = GlobalKey<FormState>();
  final _norangKey = GlobalKey<FormState>();

  File _image;
  final picker = ImagePicker();

  Future getImage() async {
    try {
      final pickedFile = await picker.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 500,
          maxWidth: 500);
      setState(() {
        if (pickedFile != null) {
          _image = File(pickedFile.path);
          fileName = _image.path.split('/').last;
          print('image selected : $fileName');
        } else {
          print('No image selected.');
          return true;
        }
      });
    } catch (e) {
      print(e);
    }
  }

  Future<bool> addImage(Map<String, String> body, String filepath) async {
    print('Coba Jalanin ini');
    String addimageUrl = Endpoint.BASE_URL + Endpoint.ADD_MEMBER;
    Map<String, String> headers = {
      'Content-Type': 'multipart/form-data',
    };
    var request = http.MultipartRequest('POST', Uri.parse(addimageUrl))
      ..fields.addAll(body)
      ..headers.addAll(headers)
      ..files.add(await http.MultipartFile.fromPath('image', filepath));
    var response = await request.send();
    if (response.statusCode == 200) {
      _onDaftarSukses(context);
      setState(() {
        isLoginrun = false;
        isDaftar = true;
      });
      setIntoSharedPreferences(isDaftar);
      return true;
    } else {
      _onDaftarGagal(context);
      setState(() {
        isLoginrun = false;
      });
      return false;
    }
  }

  @override
  void initState() {
    initConnectivity();
    checkPermission();
    super.initState();
    // Future.delayed(Duration(seconds: 3), () {
    //   _onFirstOpen(context);
    // });
  }

  checkPermission() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.location,
      Permission.storage,
      Permission.mediaLibrary,
      Permission.photos,
      Permission.photosAddOnly,
      Permission.camera
    ].request();
    print(statuses[Permission.location]);
    if (Platform.isIOS) {
      var photosStatus = await Permission.photos.status;
      if (photosStatus.isDenied) {
        var photoStatus = await Permission.photos.request();
        print("Photos Permission Status" + photosStatus.toString());
        if (photosStatus.isDenied) {
          // We didn't ask for permission yet.
        }
        if (await Permission.speech.isPermanentlyDenied) {
          // The user opted to never again see the permission request dialog for this
          // app. The only way to change the permission's status now is to let the
          // user manually enable it in the system settings.
          openAppSettings();
        }
      }
    } else {
      if (await Permission.storage.status.isDenied) {
        var storageStatus = await Permission.storage.request();
        print("Android Storage Permission Status: " + storageStatus.toString());
      }
    }

    // You can can also directly ask the permission about its status.
    if (await Permission.location.isRestricted) {
      // The OS restricts access, for example because of parental controls.
    }
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    super.dispose();
  }

  bool myInterceptor(bool stopDefaultButtonEvent, RouteInfo info) {
    if (!mounted) return false;
    print("BACK BUTTON!"); // Do some stuff.
    Navigator.pop(context);
    return true;
  }

  void notAgree() {
    Toast.show('Anda belum Menyetujui, harap di Ceklis', context);
  }

  void isAgree() {
    //Toast.show('Lanjut Pendaftaran', context);
  }

  /* Connection checking */
  Future<void> initConnectivity() async {
    ConnectivityResult result = ConnectivityResult.none;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          isInternetActive = true;
        });
        //Toast.show('WiFi Connected', context,duration: Toast.LENGTH_LONG,gravity: Toast.BOTTOM);
        break;
      case ConnectivityResult.mobile:
        setState(() {
          isInternetActive = true;
        });
        //Toast.show('Mobile Data Connected', context,duration: Toast.LENGTH_LONG,gravity: Toast.BOTTOM);
        break;
      case ConnectivityResult.none:
        setState(() {
          Toast.show('Tidak ada koneksi internet', context,
              duration: Toast.LENGTH_LONG,
              gravity: Toast.BOTTOM,
              backgroundColor: Colors.orange,
              textColor: Colors.black);
          isInternetActive = false;
        });
        break;
      default:
      // setState(() => _connectionStatus = 'Failed to get connectivity.');
      // break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(
            Icons.arrow_back_sharp,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        title: Center(
          child: Image.asset(
            'assets/logosplash.png',
            height: 70,
            width: 70,
            scale: 1.0,
          ),
        ),
        actions: <Widget>[
          Container(
              alignment: Alignment.center,
              child: Visibility(visible: true, child: Text('HAImobile'))),
        ],
      ),
      body: Stack(
        children: [
          SafeArea(
            child: Column(
              children: [
                titlePage(),
                fotoPage(),
                Expanded(child: formRegistrasi()),
              ],
            ),
          ),
          isLoginrun
              ? EasyLoader(
                  image: AssetImage(
                    'assets/logosplash.png',
                  ),
                  iconColor: Colors.white,
                )
              : Container()
        ],
      ),
    );
  }

  fotoPage() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: () async {
              getImage();
              // List<Media> res = await ImagesPicker.pick(
              //   count: 1,
              //   pickType: PickType.all,
              //   language: Language.System,
              //   maxTime: 30,
              //   maxSize: 500,
              //   cropOpt: CropOption(
              //     aspectRatio: CropAspectRatio.custom,
              //   ),
              // );
              // print(res);
              // if (res != null) {
              //   print(res.map((e) => e.path).toList());
              //   setState(() {
              //     imagepath = res[0].thumbPath;
              //     print(imagepath);
              //   });
              //   // bool status = await ImagesPicker.saveImageToAlbum(File(res[0]?.path));
              //   // print(status);
              // }
            },
            child: Container(
                child: SizedBox(
              height: 100,
              width: 100,
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(
                        width: 4,
                        color: Theme.of(context).scaffoldBackgroundColor),
                    boxShadow: [
                      BoxShadow(
                          spreadRadius: 2,
                          blurRadius: 10,
                          color: Colors.black.withOpacity(0.1),
                          offset: Offset(0, 10))
                    ],
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: _image == null
                          ? AssetImage('assets/noimageuser.png')
                          : FileImage(_image),
                      fit: BoxFit.cover,
                    )),
              ),
            )),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Container(
              child: Center(
                child: Text('Foto Profil'),
              ),
            ),
          ),
        ],
      ),
    );
  }

  titlePage() {
    return Padding(
      padding: const EdgeInsets.all(3.0),
      child: Row(
        children: [
          Text('Pendaftaran Member',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0)),
        ],
      ),
    );
  }

  formRegistrasi() {
    return SingleChildScrollView(
      child: Column(
        children: [
          /* nama */
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                border: Border.all(
                  color: Colors.blue, //                   <--- border color
                  width: 1.0,
                ),
              ),
              child: TextFormField(
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },

                key: _namaKey,
                controller: _nama,
                //obscureText: ,
                style: TextStyle(fontSize: 17, fontFamily: 'Roboto'),
                keyboardType: TextInputType.name,
                textCapitalization: TextCapitalization.sentences,
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  icon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.account_box),
                  ),
                  hintText: 'Masukan nama lengkap',
                ),
              ),
            ),
          ),
          /* email */
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                border: Border.all(
                  color: Colors.blue, //                   <--- border color
                  width: 1.0,
                ),
              ),
              child: TextFormField(
                controller: _email,
                key: _emailKey,
                style: TextStyle(fontSize: 17, fontFamily: 'Roboto'),
                keyboardType: TextInputType.emailAddress,
                textCapitalization: TextCapitalization.sentences,
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  icon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.email),
                  ),
                  hintText: 'Masukan Email',
                ),
              ),
            ),
          ),

          /* telp */
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                border: Border.all(
                  color: Colors.blue, //                   <--- border color
                  width: 1.0,
                ),
              ),
              child: TextFormField(
                controller: _telp,
                key: _telpKey,
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                inputFormatters: [new LengthLimitingTextInputFormatter(12)],
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  icon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.phone_android),
                  ),
                  hintText: 'Masukan No Telp',
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                border: Border.all(
                  color: Colors.blue, //                   <--- border color
                  width: 1.0,
                ),
              ),
              child: TextFormField(
                controller: _tipe_mobil,
                key: _unitKey,
                style: TextStyle(fontSize: 17, fontFamily: 'Roboto'),
                textCapitalization: TextCapitalization.sentences,
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  icon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.car_rental),
                  ),
                  hintText: 'Unit',
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                border: Border.all(
                  color: Colors.blue, //                   <--- border color
                  width: 1.0,
                ),
              ),
              child: TextFormField(
                controller: _tahun,
                key: _tahunKey,
                style: TextStyle(fontSize: 17, fontFamily: 'Roboto'),
                inputFormatters: [new LengthLimitingTextInputFormatter(4)],
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  icon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.calendar_today),
                  ),
                  hintText: 'Tahun',
                ),
              ),
            ),
          ),

          /* no rangka */
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                border: Border.all(
                  color: Colors.blue, //                   <--- border color
                  width: 1.0,
                ),
              ),
              child: TextFormField(
                controller: _no_rangka,
                key: _norangKey,
                style: TextStyle(fontSize: 17, fontFamily: 'Roboto'),
                textCapitalization: TextCapitalization.sentences,
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  icon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.edit),
                  ),
                  hintText: 'Masukan No Rangka',
                ),
              ),
            ),
          ),
          /* no sim */
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                border: Border.all(
                  color: Colors.blue, //                   <--- border color
                  width: 1.0,
                ),
              ),
              child: TextFormField(
                controller: _no_sim,
                key: _nosimKey,
                keyboardType: TextInputType.number,
                inputFormatters: [new LengthLimitingTextInputFormatter(14)],
                textInputAction: TextInputAction.next,
                style: TextStyle(fontSize: 17, fontFamily: 'Roboto'),
                decoration: InputDecoration(
                  border: InputBorder.none,
                  icon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.card_membership),
                  ),
                  hintText: 'Masukan No SIM',
                ),
              ),
            ),
          ),

          /* domisili */
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                border: Border.all(
                  color: Colors.blue, //                   <--- border color
                  width: 1.0,
                ),
              ),
              child: TextFormField(
                controller: _domisili,
                style: TextStyle(fontSize: 17, fontFamily: 'Roboto'),
                textCapitalization: TextCapitalization.sentences,
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  icon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.place),
                  ),
                  hintText: 'Domisili',
                ),
              ),
            ),
          ),

          /* alamat Lengkap */
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                border: Border.all(
                  color: Colors.blue, //                   <--- border color
                  width: 1.0,
                ),
              ),
              child: TextFormField(
                controller: _alamat,
                style: TextStyle(fontSize: 17, fontFamily: 'Roboto'),
                minLines: 1,
                maxLines: 2,
                keyboardType: TextInputType.streetAddress,
                textCapitalization: TextCapitalization.sentences,
                textInputAction: TextInputAction.done,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  icon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.home),
                  ),
                  hintText: 'Masukan Alamat Lengkap',
                ),
              ),
            ),
          ),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Colors.orange,
                  textStyle:
                      TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
              onPressed: () {
                openSyarat();
              },
              child: Text(
                'Klik untuk melihat Syarat dan Ketentuan',
                style: TextStyle(color: Colors.white, fontSize: 14),
              )),

          /* domisili */
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ConstrainedBox(
                constraints: BoxConstraints.tightFor(width: 200, height: 50),
                child: ElevatedButton(
                  child: Text(
                    'Registrasi',
                    style: TextStyle(
                      fontSize: 17,
                      fontFamily: 'Roboto',
                    ),
                  ),
                  style: ButtonStyle(
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(color: Colors.blueGrey)))),
                  onPressed: () async {
                    var tgl = DateFormat(" d-MMMM-yyyy h:mm:s")
                        .format(DateTime.now());
                    print('Tanggal :$tgl');
                    if (isInternetActive) {
                      print('Check');
                      setState(() {
                        formValidation(
                            _nama.text,
                            _email.text,
                            _telp.text,
                            _tipe_mobil.text,
                            _tahun.text,
                            _no_rangka.text,
                            _no_sim.text,
                            _domisili.text,
                            _alamat.text);
                        if (isValid) {
                          if (setuju) {
                            if (_image == null) {
                              Toast.show('Foto belum ditambahkan', context,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                                  duration: 1,
                                  gravity: Toast.CENTER);
                              return false;
                            }
                            isAgree();
                            isLoginrun = true;

                            Map<String, String> maps = {
                              "nama": _nama.text,
                              "telp": _telp.text,
                              "domisili": _domisili.text,
                              "email": _email.text,
                              "alamat": _alamat.text,
                              "tipe_mobil": _tipe_mobil.text,
                              "tahun": _tahun.text,
                              "no_sim": _no_sim.text,
                              "no_rangka": _no_rangka.text,
                              "status": 'Member',
                              'regdate': tgl,
                            };
                            fileName = _image.path.split('/').last;
                            addImage(maps, _image.path);
                            SharedPref.saveNamaToPreferences(_nama.text);
                          } else {
                            notAgree();
                          }
                        } else {
                          print('Masih ada Kosong');
                        }
                      });
                    } else {
                      setState(() {
                        isLoginrun = false;
                      });
                      Toast.show('Tidak ada koneksi internet!', context,
                          duration: Toast.LENGTH_LONG,
                          gravity: Toast.BOTTOM,
                          backgroundColor: Colors.orange,
                          textColor: Colors.black);
                    }
                  },
                )),
          ),
        ],
      ),
    );
  }

  formValidation(String namal, String email, String notelp, String unit,
      String tahun, String norang, String nosim, String domi, String almt) {
    if (namal.length == 0 || namal.isEmpty) {
      Toast.show('Nama tidak boleh kosong', context,
          backgroundColor: Colors.red,
          backgroundRadius: 20,
          duration: 1,
          gravity: Toast.CENTER);
      setState(() {
        isValid = false;
      });
    } else if (email.isEmpty || email.length == 0) {
      Toast.show('Email tidak boleh kosong', context,
          backgroundColor: Colors.red,
          backgroundRadius: 20,
          duration: 1,
          gravity: Toast.CENTER);
      setState(() {
        isValid = false;
      });
    } else if (notelp.isEmpty || notelp.length == 0) {
      Toast.show('No Telp tidak boleh kosong', context,
          backgroundColor: Colors.red,
          backgroundRadius: 20,
          duration: 1,
          gravity: Toast.CENTER);
      setState(() {
        isValid = false;
      });
    } else if (unit.isEmpty || unit.length == 0) {
      Toast.show('Tipe Mobil tidak boleh kosong', context,
          backgroundColor: Colors.red,
          backgroundRadius: 20,
          duration: 1,
          gravity: Toast.CENTER);
      setState(() {
        isValid = false;
      });
    } else if (tahun.isEmpty || tahun.length == 0) {
      Toast.show('Tahun Mobil tidak boleh kosong', context,
          backgroundColor: Colors.red,
          backgroundRadius: 20,
          duration: 1,
          gravity: Toast.CENTER);
      setState(() {
        isValid = false;
      });
    } else if (nosim.isEmpty || nosim.length == 0) {
      Toast.show(
          'Jika belum punya SIM,\n Silahkan isi no asal dan info kami jika \n sudah memiliki SIM',
          context,
          backgroundColor: Colors.black,
          backgroundRadius: 20,
          textColor: Colors.white,
          duration: 3,
          gravity: Toast.CENTER);
      setState(() {
        isValid = false;
      });
    } else if (norang.isEmpty || norang.length == 0) {
      Toast.show('No Rangka tidak boleh kosong', context,
          backgroundColor: Colors.red,
          backgroundRadius: 20,
          duration: 1,
          gravity: Toast.CENTER);
      setState(() {
        isValid = false;
      });
    } else if (domi.isEmpty || domi.length == 0) {
      Toast.show('Domisili tidak boleh kosong', context,
          backgroundColor: Colors.red,
          backgroundRadius: 20,
          duration: 1,
          gravity: Toast.CENTER);
      setState(() {
        isValid = false;
      });
    } else if (almt.isEmpty || almt.length == 0) {
      Toast.show('Alamat tidak boleh kosong', context,
          backgroundColor: Colors.red,
          backgroundRadius: 20,
          duration: 1,
          gravity: Toast.CENTER);
      setState(() {
        isValid = false;
      });
    } else {
      setState(() {
        isValid = true;
      });
    }
  }

  /* test */

  /* PROSES DAFTAR */
  proccedRequest(
      String nama,
      String telp,
      String domisili,
      String email,
      String alamat,
      String tipemobil,
      String tahun,
      String nosim,
      String norangka,
      String paths) async {
    //callLoading();
    try {
      //var client = Dio(BaseOptions(baseUrl: Endpoint.BASE_URL));
      var formData = FormData.fromMap({
        "nama": nama,
        "telp": telp,
        "domisili": domisili,
        "email": email,
        "alamat": alamat,
        "tipe_mobil": tipemobil,
        "tahun": tahun,
        "no_sim": nosim,
        "no_rangka": norangka,
        "image": await MultipartFile.fromFile(paths, filename: fileName),
        "status": 'Member',
      });
      // Map<String, dynamic> imageData = {
      //         "image": await MultipartFile.fromFile(path),
      //     };
      Dio _dio = new Dio();
      var send = await _dio.post(Endpoint.BASE_URL + Endpoint.ADD_MEMBER,
          data: formData, options: Options(contentType: 'multipart/form-data'));
      print('ygdikirim : $send');
      if (send.statusCode == 200) {
        setState(() {
          isLoginrun = false;
          _onDaftarSukses(context);
        });
      }
    } on DioError catch (e) {
      if (e.response.statusCode == 500) {
        setState(() {
          isLoginrun = false;
          _onDaftarGagal(context);
        });
        print('Error Response');
        Toast.show("Server ERROR", context,
            duration: Toast.LENGTH_SHORT, backgroundColor: Colors.deepOrange);
      }
      if (e.response.statusCode == 404) {
        setState(() {
          isLoginrun = false;
          _onDaftarGagal(context);
        });
        print('Server Error');
      }
      if (e.type == DioErrorType.connectTimeout) {
        print('Device Offline');
        setState(() {
          isLoginrun = false;
          _onDaftarGagal(context);
        });
      }
      print('Command ' + e.response.statusCode.toString());
    }
  }

  void openSyarat() {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        builder: (context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter state) {
            return Wrap(children: <Widget>[
              Container(
                child: Container(
                  height: 420,
                  decoration: new BoxDecoration(
                      color: Colors.white,
                      borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(25.0),
                          topRight: const Radius.circular(25.0))),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Center(
                          child: Text(
                            'SYARAT & KETENTUAN',
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      Divider(
                        color: Colors.grey,
                        height: 10,
                      ),
                      Container(
                        child: Column(
                          children: [
                            ListTile(
                              leading: Text(
                                '# >',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 22),
                              ),
                              title: new Text(
                                'Untuk bergabung di klub ini anda harus memiliki Unit.',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black54),
                              ),
                              onTap: () {
                                Navigator.pop(context);
                              },
                            ),
                            ListTile(
                              leading: Text(
                                '# >',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 22),
                              ),
                              title: new Text(
                                'Tidak diperbolehkan jika anda sudah tergabung di klub lain atau klub sejenis.',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.red),
                              ),
                              onTap: () {
                                Navigator.pop(context);
                              },
                            ),
                            ListTile(
                              leading: Text(
                                '# >',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 22),
                              ),
                              title: new Text(
                                'Selalu menjaga nama baik Klub Hyundai Accent Indonesia',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black54),
                              ),
                              onTap: () {
                                Navigator.pop(context);
                              },
                            ),
                            ListTile(
                              leading: Text(
                                '# >',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 22),
                              ),
                              title: new Text(
                                'Selalu mentaati peraturan lalu lintas dalam berkendara.',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black54),
                              ),
                              onTap: () {
                                Navigator.pop(context);
                              },
                            ),
                            ListTile(
                              leading: Text(
                                '# >',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 22),
                              ),
                              title: new Text(
                                'Selalu menjalin silaturahmi sesama member maupun pengurus.',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black54),
                              ),
                              onTap: () {
                                Navigator.pop(context);
                              },
                            ),
                            Row(
                              children: [
                                Material(
                                  child: Checkbox(
                                    checkColor: Colors.white,
                                    activeColor: Colors.blueAccent,
                                    value: setuju,
                                    onChanged: (value) {
                                      state(() {
                                        setuju = value ?? false;
                                      });
                                    },
                                  ),
                                ),
                                Text(
                                  'Saya setuju dengan syarat dan Ketentuan di atas.',
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontSize: 13,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ]);
          });
        });
  }

  /* alert success */
  _onDaftarSukses(context) {
    Alert(
      context: context,
      type: AlertType.success,
      title: "PENDAFTARAN BERHASIL",
      desc:
          "Terima kasih sudah mendaftar,\n silahkan hubungi Bendahara Umum \n untuk Keperluan atribut.",
      buttons: [
        DialogButton(
          child: Text(
            "SELESAI",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {
            setState(() {
              isLoginrun = false;
              formClearing();
            });
            Navigator.pop(context);
          },
          width: 120,
        )
      ],
      alertAnimation: fadeAlertAnimation,
    ).show();
  }

  Widget fadeAlertAnimation(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    return Align(
      child: FadeTransition(
        opacity: animation,
        child: child,
      ),
    );
  }

  formClearing() {
    setState(() {
      _nama.clear();
      _telp.clear();
      _domisili.clear();
      _alamat.clear();
      _email.clear();
      _tipe_mobil.clear();
      _tahun.clear();
      _no_sim.clear();
      _no_rangka.clear();
      _image = null;
    });
  }

  _onFirstOpen(context) {
    Alert(
      context: context,
      type: AlertType.warning,
      title: "Informasi",
      desc: "Silahkan isi data diri anda!",
      buttons: [
        DialogButton(
          child: Text(
            "TUTUP",
            style: TextStyle(color: Colors.white, fontSize: 17),
          ),
          onPressed: () {
            setState(() {
              isLoginrun = false;
            });
            Navigator.pop(context);
          },
          width: 120,
        )
      ],
      alertAnimation: fadeAlertAnimation2,
    ).show();
  }

  _onDaftarGagal(context) {
    Alert(
      context: context,
      type: AlertType.error,
      title: "PENDAFTARAN GAGAL",
      desc: "Pendaftaran gagal, silahkan coba lagi nanti!",
      buttons: [
        DialogButton(
          child: Text(
            "TUTUP",
            style: TextStyle(color: Colors.white, fontSize: 17),
          ),
          onPressed: () {
            setState(() {
              isLoginrun = false;
            });
            Navigator.pop(context);
          },
          width: 120,
        )
      ],
      alertAnimation: fadeAlertAnimation2,
    ).show();
  }

  Widget fadeAlertAnimation2(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    return Align(
      child: FadeTransition(
        opacity: animation,
        child: child,
      ),
    );
  }

  List<String> menuKategori = [
    "assets/member.png",
    "assets/rss.png",
    "assets/twocar.png",
    "assets/adduser.png",
    "assets/shop.png",
    "assets/chat.png",
  ];
  List<String> menuName = [
    "Member",
    "Story",
    "Kopdar",
    "Daftar",
    "Marketplace",
    "Grup Chat",
  ];

  List imageSlider = ["assets/"];
  void setIntoSharedPreferences(bool daftar) async {
    prefs = await SharedPreferences.getInstance();
    await prefs.setBool("isDaftar", daftar);
  }
}
