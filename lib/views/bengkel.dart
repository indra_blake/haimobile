import 'package:badges/badges.dart';
import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:url_launcher/url_launcher.dart';

class Bengkel extends StatefulWidget {
  //const Bengkel({ Key? key }) : super(key: key);

  @override
  _BengkelState createState() => _BengkelState();
}

class _BengkelState extends State<Bengkel> {
  final GlobalKey<ExpansionTileCardState> cardA = new GlobalKey();
  final GlobalKey<ExpansionTileCardState> cardB = new GlobalKey();
  final ButtonStyle flatButtonStyle = TextButton.styleFrom(
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(4.0)),
    ),
  );
  bool err = false;
  String msgErr = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(
            Icons.arrow_back_sharp,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        title: Center(
          child: Image.asset(
            'assets/logosplash.png',
            height: 80,
            width: 80,
            scale: 1.0,
          ),
        ),
        actions: <Widget>[
          Container(
              alignment: Alignment.center,
              child: Visibility(visible: true, child: Text('HAIMobile'))),
          // IconButton(
          //   icon: Icon(Icons.navigate_next),
          //   onPressed: (){

          //   },
          // ),
          // IconButton(
          //   icon: Icon(Icons.refresh),
          //   onPressed: () {
          //     // if (isSampleDoc) {
          //     //   _pdfController.loadDocument(
          //     //       PdfDocument.openAsset('assets/dummy.pdf'));
          //     // } else {
          //     //   _pdfController.loadDocument(
          //     //       PdfDocument.openAsset('assets/sample.pdf'));
          //     // }
          //     // isSampleDoc = !isSampleDoc;
          //   },
          // )
        ],
      ),
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              // Text('Data Bengkel')
              titlePage(),
              Expanded(
                child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListView.builder(
                      itemCount: titles.length,
                      itemBuilder: (BuildContext context, index) {
                        print('indexnya: $index');
                        // for (var data in _kopdarlist) {
                        //   dat = data.titleKopdar;
                        //   dat2 = data.subTitle;
                        //   dat3 = data.isi;
                        //   print("Datanya : $dat, $dat2, $dat3");
                        // }
                        return ExpansionTileCard(
                          //key: cardA,

                          leading: CircleAvatar(child: Text('B')),
                          title: Text(titles[index]),
                          subtitle: Text(subtitles[index]),

                          //trailing: Text('19:30'),
                          //animateTrailing: true,
                          children: <Widget>[
                            Divider(
                              thickness: 1.0,
                              height: 1.0,
                            ),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 16.0,
                                  vertical: 8.0,
                                ),
                                child: Text(
                                  isi[index],
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText2
                                      .copyWith(fontSize: 16),
                                ),
                              ),
                            ),
                            ButtonBar(
                              alignment: MainAxisAlignment.spaceAround,
                              buttonHeight: 52.0,
                              buttonMinWidth: 90.0,
                              children: <Widget>[
                                Column(
                                  children: [
                                    Text('Status'),
                                    Badge(
                                      toAnimate: true,
                                      animationType: BadgeAnimationType.scale,
                                      animationDuration:
                                          Duration(milliseconds: 800),
                                      shape: BadgeShape.square,
                                      badgeColor: Colors.green,
                                      borderRadius: BorderRadius.circular(8),
                                      badgeContent: Text(' Bagus',
                                          style:
                                              TextStyle(color: Colors.white)),
                                    ),
                                  ],
                                ),
                                TextButton(
                                  style: flatButtonStyle,
                                  onPressed: () {
                                    _launchWhatsapp();
                                   
                                    cardB.currentState?.expand();
                                  },
                                  child: Column(
                                    children: <Widget>[
                                      Icon(Icons.chat),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 2.0),
                                      ),
                                      Text('Hubungi'),
                                    ],
                                  ),
                                ),
                                TextButton(
                                  style: flatButtonStyle,
                                  onPressed: () {
                                    _launchMaps(titles[index]);
                                    cardB.currentState?.collapse();
                                  },
                                  child: Column(
                                    children: <Widget>[
                                      Icon(Icons.directions),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 2.0),
                                      ),
                                      Text('Arahkan'),
                                    ],
                                  ),
                                ),
                                // TextButton(
                                //   style: flatButtonStyle,
                                //   onPressed: () {
                                //     cardB.currentState?.toggleExpansion();
                                //   },
                                //   child: Column(
                                //     children: <Widget>[
                                //       Icon(Icons.skip_next),
                                //       Padding(
                                //         padding: const EdgeInsets.symmetric(
                                //             vertical: 2.0),
                                //       ),
                                //       Text('Skip'),
                                //     ],
                                //   ),
                                // ),
                              ],
                            ),
                          ],
                        );
                      },
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }

  titlePage() {
    return Padding(
      padding: const EdgeInsets.only(top: 5, left: 15),
      child: Row(
        children: [
          Text('Bengkel Rekomendasi',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0)),
        ],
      ),
    );
  }

  final titles = ["Bengkel Mesin", "Bengkel AC"];
  final subtitles = ["Raja Jaya Motor", "Nasuha Cool AC Specialist"];
  final isi = [
    "BSD Auto Part, Jl. Letnan Sutopo No.11, Lengkong Gudang Tim., Kec. Serpong, Kota Tangerang Selatan, Banten 15310",
    "Jl. Sultan Agung, RT.001/RW.022, Kota Baru, Kec. Bekasi Barat., Kota Bks, Jawa Barat 17132"
  ];

  _launchWhatsapp() async {
    const url =
        "https://wa.me/6281299261743/?text=Saya%20tertarik%20dengan%20mobil%20Anda%20yang%20dijual";
    await launch(url);
  }

  _launchMaps(var ok)async {
    if(ok == 'Bengkel Mesin'){
      MapsLauncher.launchCoordinates(
                    -6.2952067, 106.6777969, 'Raja Jaya Motor');
    }else{
       MapsLauncher.launchCoordinates(
                    -6.2158044, 106.9731983, 'Nasuha Cool AC Specialist');
    }
    
  }
}
