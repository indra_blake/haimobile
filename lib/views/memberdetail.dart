import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:cached_network_image/cached_network_image.dart';
import "package:flutter/material.dart";
import 'package:mobile/model/memberModel.dart';

class MemberDetail extends StatefulWidget {
  final MemberModel _user;

  MemberDetail(this._user);
  @override
  _MemberDetail createState() => _MemberDetail();
}
  class _MemberDetail extends State<MemberDetail> {


  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add(myInterceptor);
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    super.dispose();
  }
  bool myInterceptor(bool stopDefaultButtonEvent, RouteInfo info) {
    print("BACK BUTTON!"); // Do some stuff.
    Navigator.pop(context);
    return true;
  }  

  @override
  Widget build(BuildContext context) {
    var ku;
    if (widget._user.jabatan == 'Ketua Umum' ||
        widget._user.jabatan == 'Sekretaris' ||
        widget._user.jabatan == 'HUMAS') {
      ku = Colors.red;
    } else {
      ku = Colors.blue;
    }
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(
            Icons.arrow_back_sharp,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(widget._user.name),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(16),

              /// This is the important part, we need [Hero] widget with unique tag but same as Hero's tag in [User] widget.
              child: Hero(
                tag: "avatar_" + widget._user.id.toString(),
                child: CircleAvatar(
                  radius: 100,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: CachedNetworkImage(
                      imageUrl: widget._user.avatar,
                      imageBuilder: (context, imageProvider) => Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover,
                              // colorFilter: ColorFilter.mode(
                              //     Colors.red, BlendMode.colorBurn)
                                  ),
                        ),
                      ),
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ),
                ),
              ),
            ),
            Text(
              widget._user.name,
              style: TextStyle(fontSize: 22),
            ),
            Text(widget._user.jabatan,
                style: TextStyle(
                    color: widget._user.jabatan != null ? ku : Colors.blue,
                    fontSize: 22,
                    fontWeight: FontWeight.bold)),
            dataDetailMember(),
          ],
        ),
      ),
    );
  }

  dataDetailMember() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        child: Card(
          semanticContainer: true,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Nopung',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 22.0)),
                    Text('Unit',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 22.0)),
                    Text('Chapter',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 22.0)),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(widget._user.nopung,
                          style: TextStyle(
                            color: Colors.black,
                          )),
                      Text(widget._user.chapter,
                          style: TextStyle(
                            color: Colors.black,
                          )),
                      Text(widget._user.unit,
                          style: TextStyle(
                            color: Colors.black,
                          )),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Bergabung :',
                          style: TextStyle(
                            color: Colors.black,
                          )),
                      Text(widget._user.joinDate,
                          style: TextStyle(
                            color: Colors.black,
                          )),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  
}
