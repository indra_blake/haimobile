import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mobile/views/memberdetail.dart';
import 'package:mobile/model/memberModel.dart';
import 'package:mobile/views/memberdetail2.dart';

class MemberAction extends StatefulWidget {
  final MemberModel _user;
  MemberAction(this._user);
  
  @override
  _MemberActionState createState() => _MemberActionState();
}

class _MemberActionState extends State<MemberAction> {
  List<String> newList = [];
  @override
  void initState(){
    newList.add(widget._user.avatar);
    print('DataimageList : $newList');
    //checkImageValid();
    super.initState();
  }

  // checkImageValid(){
  //   for(var img in widget._user.avatar)
  // }


  @override
  Widget build(BuildContext context) {
    var ku;
    Image ganti;
    // if (_user.jabatan == 'Ketua Umum' ||
    //     _user.jabatan == 'Sekretaris' ||
    //     _user.jabatan == 'HUMAS') {
    //   ku = Colors.red;
    // } else {
    //   ku = Colors.blue;
    // }

    /// [InkWell] to listen to tap and give ripples effect
    return Container(
      color: Colors.white,
      child: InkWell(
        // onTap: () => Navigator.push(context,
        //     MaterialPageRoute(builder: (context) => MemberDetail2('test'))),
        child: Container(
          /// Give nice padding
          child: Container(
            child: Card(
              semanticContainer: true,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              elevation: 4,
              margin: EdgeInsets.all(5),
              child: Column(
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Padding(
                        padding:
                            const EdgeInsets.only(top: 12, bottom: 12, left: 4),
                        child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 30,
                              child: Image.asset("assets/noimageuser.png")),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8,top: 2),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(bottom: 4),
                              child: Text(widget._user.name,style: TextStyle(color: Colors.black,fontWeight: FontWeight.w500,fontSize: 18),),
                            ),
                            Text(widget._user.nopung),
                            Text(widget._user.chapter),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
