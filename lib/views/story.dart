import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:like_button/like_button.dart';
import 'package:mobile/services/endpoint.dart';
import 'dart:math' as math;
import 'package:http/http.dart' as http;
import 'package:mobile/views/dashboard.dart';
import 'package:mobile/model/storyModel.dart';
import 'package:mobile/views/story_action.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:path/path.dart' as path;
import 'package:readmore/readmore.dart';
import 'package:share/share.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:permission_handler/permission_handler.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class Story extends StatefulWidget {
  @override
  _StoryState createState() => _StoryState();
}

class _StoryState extends State<Story> {
  final fireStoreDB = FirebaseFirestore.instance;
  StreamController _postsController;
  final _firebaseAuth = FirebaseAuth.instance;
  final _transformationController = TransformationController();
  TapDownDetails _doubleTapDetails;
  FirebaseStorage storageFB = FirebaseStorage.instance;
  SharedPreferences prefs;
  File _image;
  File imagefile;
  bool isEditImage = false;
  String fileName;
  String emailOwner;
  String postowner;
  var nama;
  var nopung;
  var unit;
  var imageDataServer;
  var imageurl;
  var islike;
  var iscomment;
  var owners;
  var timepost;
  var titlepost;
  var avatar;
  var statusmember;
  var commentuid;
  var tgl;
  var emailposter;
  var idmember;
  bool isImageFromLibrary = false;
  bool isImageFromCamera = false;
  bool isPostOwner = false;
  bool menuenable = false;
  bool isLike = false;
  final picker = ImagePicker();
  String _documentId;
  TextEditingController _descController = TextEditingController();
  final List<StoryModel> _storylist = [];
  CollectionReference _dataStory =
      FirebaseFirestore.instance.collection('Story');

  Future getImage() async {
    try {
      final pickedFile = await picker.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 500,
          maxWidth: 500);
      setState(() {
        if (pickedFile != null) {
          _image = File(pickedFile.path);
          fileName = _image.path.split('/').last;
          print('image selected : $_image');
          imagefile = _image;
        } else {
          print('No image selected.');
          return true;
        }
      });
    } catch (e) {
      print(e);
    }
  }

  Future<bool> addImage(Map<String, String> body, String filepath) async {
    print('Coba Jalanin ini');
    String addimageUrl = Endpoint.BASE_URL + Endpoint.ADD_STORIES;
    Map<String, String> headers = {
      'Content-Type': 'multipart/form-data',
    };
    var request = http.MultipartRequest('POST', Uri.parse(addimageUrl))
      ..fields.addAll(body)
      ..headers.addAll(headers)
      ..files.add(await http.MultipartFile.fromPath('image', filepath));
    var response = await request.send();
    if (response.statusCode == 200) {
      //_onDaftarSukses(context);
      Toast.show('Berhasil posting story', context);
      setState(() {
        loadPosts();
      });
      return true;
    } else {
      //_onDaftarGagal(context);
      Toast.show('Gagal posting story \n atau anda belum registrasi!', context);
      print('Statuscode : ${response.statusCode}');
      return false;
    }
  }

  Future<bool> addNoImage(
      String nama, String nopung, String unit, String email) async {
    String addimageUrl = Endpoint.BASE_URL + Endpoint.EDIT_MEMBER + email;
    print('Coba Jalanin ini $addimageUrl');
    var url = Uri.parse(addimageUrl);
    var response = await http.post(url,
        body: {'nama': '$nama', 'nopung': '$nopung', 'tipe_mobil': '$unit'});
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      //_onDaftarSukses(context);
      Toast.show('Berhasil posting story', context);
      // setState(() {
      //   isLoginrun = false;
      // });
      return true;
    } else {
      //_onDaftarGagal(context);
      Toast.show('Gagal posting story!', context);
      // setState(() {
      //   isLoginrun = false;
      // });
      return false;
    }
  }

  Future<Null> getSharedPrefs() async {
    prefs = await SharedPreferences.getInstance();
    postowner = prefs.getString("uid");
    emailOwner = prefs.getString('email');
    print('UID $postowner');
    if (postowner == null || emailOwner == null) {
      setState(() {
        _onAlertPost(context);
        print('Belum daftar akun');
      });
    } else {
      getMemberSingle(emailOwner);
    }
  }

  /* ambil datanya dlu */
  Future<Null> getMemberSingle(String email) async {
    String url = Endpoint.BASE_URL + Endpoint.MEMBER_SINGLE + email;
    final response = await http.get(Uri.parse(url));
    var responseData = json.decode(response.body);

    if (response.statusCode == 200) {
      setState(() {
        idmember = responseData[0]['id_member'];

        nama = responseData[0]['nama'];
        emailposter = responseData[0]['email'];
        nopung = responseData[0]['nopung'];
        unit = responseData[0]['tipe_mobil'];
        imageDataServer = responseData[0]['avatar'];
        statusmember = responseData[0]['status_member'];
        print('idmember : $idmember');
        
      });
    } else {
      print('Bukan Member/Belum daftar');
    }
  }

  Future fetchPost() async {
    String url = Endpoint.BASE_URL + Endpoint.ALL_STORIES;
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to load post');
    }
  }

  loadPosts() async {
    fetchPost().then((res) async {
      _postsController.add(res);
      return res;
    });
  }

  checkPermission() async {
    var status = await Permission.photos.status;

    if (status.isDenied) {
      // We didn't ask for permission yet.
    }

    // You can can also directly ask the permission about its status.
    if (await Permission.location.isRestricted) {
      // The OS restricts access, for example because of parental controls.
    }
  }

  void _handleDoubleTap() {
    if (_transformationController.value != Matrix4.identity()) {
      _transformationController.value = Matrix4.identity();
    } else {
      final position = _doubleTapDetails.localPosition;
      // For a 3x zoom
      _transformationController.value = Matrix4.identity()
        ..translate(-position.dx * 2, -position.dy * 2)
        ..scale(3.0);
      // Fox a 2x zoom
      // ..translate(-position.dx, -position.dy)
      // ..scale(2.0);
    }
  }

  void _handleDoubleTapDown(TapDownDetails details) {
    _doubleTapDetails = details;
  }

  @override
  void initState() {
    getSharedPrefs();
    checkPermission();
    _postsController = new StreamController();
    loadPosts();
    getSystemDate();
    super.initState();
  }

  String getSystemDate() {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    var skrg = dateFormat.format(DateTime.now());
    return skrg;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          leading: new IconButton(
            icon: new Icon(
              Icons.arrow_back_sharp,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          backgroundColor: Colors.white,
          elevation: 0,
          title: new Center(
            child: Image.asset(
              'assets/logosplash.png',
              height: 80,
              width: 80,
              scale: 1.0,
            ),
          ),
          actions: <Widget>[
            Container(
                alignment: Alignment.center,
                child: Visibility(visible: true, child: Text('HAImobile'))),
          ],
        ),
        body:
            //storyAllAdapter(),
            storyAdapter(),
      ),
    );
  }

  storyAllAdapter() {
    return StreamBuilder<QuerySnapshot>(
        stream: fireStoreDB
            .collection('Story')
            //.where('timepost', isGreaterThan: tgl)
            .orderBy('timepost', descending: true)
            .snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
          if (streamSnapshot.hasData) {
            return Column(
              children: [
                Expanded(
                  child: ListView.builder(
                      itemCount: streamSnapshot.data.docs.length,
                      itemBuilder: (context, index) {
                        final DocumentSnapshot documentSnapshot =
                            streamSnapshot.data.docs[index];
                        var ambildlu = documentSnapshot['timepost'];
                        var poster = documentSnapshot['owner'];
                        //DateFormat dateFormat = DateFormat(ambildlu);
                        DateTime time1 = DateTime.parse(ambildlu);
                        var timeag = timeago.format(time1, locale: 'id');
                        print('Waktu $timeag');
                        DocumentSnapshot documentSnapshot2 =
                            streamSnapshot.data.docs.elementAt(index);
                        String _documentId = documentSnapshot2.id;
                        if (postowner == poster) {
                          print('yg punya post');
                          menuenable = true;
                        } else {
                          print('bukan yg punya post');
                          menuenable = false;
                        }
                        // Add a new locale messages
                        timeago.setLocaleMessages('id', timeago.IdMessages());
                        return Container(
                          color: Colors.white,
                          child: InkWell(
                            onLongPress: () {
                              DocumentSnapshot documentSnapshot =
                                  streamSnapshot.data.docs.elementAt(index);
                              String _documentId = documentSnapshot.id;
                              print('LongPress $_documentId');
                            },
                            child: Container(
                              /// Give nice padding
                              child: Container(
                                child: Card(
                                  semanticContainer: true,
                                  clipBehavior: Clip.antiAliasWithSaveLayer,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  elevation: 4,
                                  margin: EdgeInsets.all(5),
                                  child: Column(
                                    children: [
                                      // Row(
                                      //   mainAxisAlignment: MainAxisAlignment.end,
                                      //   children: [
                                      //     Icon(Icons.more_vert),
                                      //   ],
                                      // ),
                                      Row(
                                        //smainAxisSize: MainAxisSize.max,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 12, bottom: 12, left: 4),
                                            child: Hero(
                                              tag: "avatar_" +
                                                  documentSnapshot['owner'],
                                              child: CircleAvatar(
                                                radius: 30,
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(60),
                                                  child: CachedNetworkImage(
                                                    imageUrl: documentSnapshot[
                                                        'avatar'],
                                                    placeholder: (context,
                                                            url) =>
                                                        CircularProgressIndicator(
                                                      color: Colors.red,
                                                    ),
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            Icon(Icons.error),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 10),
                                                child: Text(
                                                  documentSnapshot[
                                                      'namaposter'],
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 10),
                                                child: Text(
                                                  timeag,
                                                  style:
                                                      TextStyle(fontSize: 12),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Expanded(
                                            child: Row(
                                              //mainAxisSize: MainAxisSize.max,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                //cekMenu(),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Flexible(
                                            child: Padding(
                                              /// Give name text a Padding
                                              padding:
                                                  const EdgeInsets.all(10.0),
                                              child: ReadMoreText(
                                                documentSnapshot['title'],
                                                trimLines: 3,
                                                style: TextStyle(
                                                    color: Colors.black),
                                                colorClickableText: Colors.pink,
                                                trimMode: TrimMode.Line,
                                                trimCollapsedText:
                                                    '(Selengkapnya..)',
                                                trimExpandedText: '(Tutup)',
                                                lessStyle: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.grey),
                                                moreStyle: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.blue),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                      Padding(

                                          /// Give name text a Padding
                                          padding: const EdgeInsets.all(6.0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.stretch,
                                            children: [
                                              GestureDetector(
                                                onDoubleTapDown:
                                                    _handleDoubleTapDown,
                                                onDoubleTap: _handleDoubleTap,
                                                onTap: () {
                                                  // PhotoView(imageProvider: NetworkImage(_user.imagePic),
                                                  // tightMode: true,
                                                  // );
                                                  print('GridClick');
                                                },
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.only(
                                                    topLeft:
                                                        Radius.circular(8.0),
                                                    topRight:
                                                        Radius.circular(8.0),
                                                  ),
                                                  child: CachedNetworkImage(
                                                    imageUrl: documentSnapshot[
                                                        'imageurl'],
                                                    placeholder:
                                                        (context, url) =>
                                                            Center(
                                                      child: SizedBox(
                                                        height: 30,
                                                        width: 30,
                                                        child:
                                                            CircularProgressIndicator(
                                                          color: Colors.red,
                                                        ),
                                                      ),
                                                    ),
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            Icon(Icons.error),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          )),
                                      Row(
                                        // mainAxisSize: MainAxisSize.max,
                                        //mainAxisAlignment: MainAxisAlignment.spaceAround,
                                        children: [
                                          SizedBox(
                                            height: 40,
                                            width: 40,
                                            child: IconButton(
                                                onPressed: () {},
                                                icon: new Tab(
                                                  icon: Image.asset(
                                                      "assets/heart.png"),
                                                )),
                                          ),
                                          SizedBox(
                                            height: 40,
                                            width: 40,
                                            child: IconButton(
                                                onPressed: () {},
                                                icon: new Tab(
                                                  icon: Image.asset(
                                                      "assets/comment.png"),
                                                )),
                                          ),
                                          SizedBox(
                                              height: 40,
                                              width: 40,
                                              child: IconButton(
                                                  onPressed: () {},
                                                  icon: new Tab(
                                                    icon: Image.asset(
                                                        "assets/share.png"),
                                                  ))),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                      }),
                ),
                Container(
                  height: 50,
                  // decoration: BoxDecoration(
                  //   borderRadius: BorderRadius.circular(40),
                  //             color: Color(0x00000000),
                  //             // boxShadow: [
                  //             //   BoxShadow(color: Colors.transparent),
                  //             // ],
                  //           ),
                  alignment: FractionalOffset.bottomCenter,
                  child: GestureDetector(
                    onTap: () {
                      if (postowner == null || idmember == null) {
                        _onAlertPost(context);
                      } else {
                        showModalBottomSheet(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.vertical(
                                    top: Radius.circular(25.0))),
                            backgroundColor: Colors.white,
                            context: context,
                            isScrollControlled: true,
                            builder: (context) {
                              return StatefulBuilder(builder:
                                  (BuildContext context, StateSetter picState) {
                                return FractionallySizedBox(
                                  heightFactor: 0.7,
                                  child: Wrap(
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        // mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: TextField(
                                              maxLines: null,
                                              decoration: InputDecoration(
                                                  hintText:
                                                      'Apa yang anda pikirkan'),
                                              // autofocus: true,
                                              controller: _descController,
                                            ),
                                          ),
                                        ],
                                      ),
                                      GestureDetector(
                                        onTap: () async {
                                          showModalBottomSheet(
                                              context: context,
                                              builder: (BuildContext bc) {
                                                return SafeArea(
                                                  child: Container(
                                                    child: new Wrap(
                                                      children: <Widget>[
                                                        new ListTile(
                                                            leading: new Icon(Icons
                                                                .photo_library),
                                                            title: new Text(
                                                                'Galeri Foto'),
                                                            onTap: () async {
                                                              setState(() {
                                                                isEditImage =
                                                                    true;
                                                              });
                                                              FocusManager
                                                                  .instance
                                                                  .primaryFocus
                                                                  ?.unfocus();
                                                              Navigator.of(
                                                                      context)
                                                                  .pop();
                                                              try {
                                                                final pickedFile = await picker.pickImage(
                                                                    source: ImageSource
                                                                        .gallery,
                                                                    imageQuality:
                                                                        100,
                                                                    maxHeight:
                                                                        500,
                                                                    maxWidth:
                                                                        500);
                                                                picState(() {
                                                                  if (pickedFile !=
                                                                      null) {
                                                                    _image = File(
                                                                        pickedFile
                                                                            .path);
                                                                    fileName = _image
                                                                        .path
                                                                        .split(
                                                                            '/')
                                                                        .last;
                                                                    print(
                                                                        'image selected : $_image');
                                                                    picState(
                                                                        () {
                                                                      isEditImage =
                                                                          true;
                                                                      imagefile =
                                                                          _image;
                                                                    });
                                                                  } else {
                                                                    print(
                                                                        'No image selected.');
                                                                    return true;
                                                                  }
                                                                });
                                                              } catch (e) {
                                                                print(e);
                                                              }
                                                            }),
                                                        new ListTile(
                                                          leading: new Icon(Icons
                                                              .photo_camera),
                                                          title: new Text(
                                                              'Kamera'),
                                                          onTap: () async {
                                                            FocusManager
                                                                .instance
                                                                .primaryFocus
                                                                ?.unfocus();
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                            if (!mounted)
                                                              return;
                                                            try {
                                                              final pickedFile =
                                                                  await picker.pickImage(
                                                                      source:
                                                                          ImageSource
                                                                              .camera,
                                                                      imageQuality:
                                                                          100,
                                                                      maxHeight:
                                                                          500,
                                                                      maxWidth:
                                                                          500);
                                                              picState(() {
                                                                if (pickedFile !=
                                                                    null) {
                                                                  _image = File(
                                                                      pickedFile
                                                                          .path);
                                                                  fileName = _image
                                                                      .path
                                                                      .split(
                                                                          '/')
                                                                      .last;
                                                                  print(
                                                                      'image selected : $_image');
                                                                  picState(() {
                                                                    isEditImage =
                                                                        true;
                                                                    imagefile =
                                                                        _image;
                                                                  });
                                                                } else {
                                                                  print(
                                                                      'No image selected.');
                                                                  return true;
                                                                }
                                                              });
                                                            } catch (e) {
                                                              print(e);
                                                            }
                                                          },
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                );
                                              });
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            children: [
                                              Icon(Icons.add_a_photo_outlined),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 8),
                                                child: Text('Tambahkan Foto'),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      imagefile != null
                                          ? Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Container(
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                height: 250,
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                    fit: BoxFit.cover,
                                                    image: FileImage(imagefile),
                                                  ),
                                                ),
                                              ),
                                            )
                                          : Center(
                                              child: Image.asset(
                                                  'assets/noimage.png')),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Center(
                                          child: SizedBox(
                                              width: 150,
                                              height: 50,
                                              child: ElevatedButton(
                                                  onPressed: () {
                                                    uploadFile();
                                                    setState(() {
                                                      Navigator.pop(context);
                                                    });
                                                    print('post');
                                                  },
                                                  child: Text('Post'))),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              });
                            });
                      }
                    },
                    child: ClipRRect(
                      child: SizedBox(
                        height: 50,
                        width: 50,
                        child: Container(
                            height: 48,
                            width: 48,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(40),
                              color: Colors.grey[200],
                              boxShadow: [
                                BoxShadow(color: Colors.grey[100]),
                              ],
                            ),
                            child: Icon(Icons.add_a_photo)),
                      ),
                    ),
                  ),
                ),
              ],
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  storyAdapter() {
    return StreamBuilder(
        stream: _postsController.stream,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return new Center(
                child: CircularProgressIndicator(
              color: Colors.blue,
            ));
          }
          return Column(
            children: [
              Expanded(
                child: ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      var post = snapshot.data[index];
                      var em = snapshot.data[index];
                      emailposter = em;
                      print('Emailposter $emailposter');
                      var ambildlu = post['post_date'];
                      DateTime time1 = DateTime.parse(ambildlu);
                      var timeag = timeago.format(time1, locale: 'id');
                      timeago.setLocaleMessages('id', timeago.IdMessages());

                      return Container(
                        color: Colors.white,
                        child: InkWell(
                          // onDoubleTap: (){
                          //   var lokasidata = snapshot.data.elementAt(index);
                          //  _documentId = lokasidata['email'];
                          //   print('Likes:  $_documentId');
                          // },
                          child: Container(
                            /// Give nice padding
                            child: Container(
                              child: Card(
                                semanticContainer: true,
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                elevation: 4,
                                margin: EdgeInsets.all(5),
                                child: Column(
                                  children: [
                                    // Row(
                                    //   mainAxisAlignment: MainAxisAlignment.end,
                                    //   children: [
                                    //     Icon(Icons.more_vert),
                                    //   ],
                                    // ),
                                    Row(
                                      //smainAxisSize: MainAxisSize.max,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              top: 12, bottom: 12, left: 4),
                                          child: Hero(
                                            tag: "avatar_" + post['nama'],
                                            child: CircleAvatar(
                                              radius: 30,
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(60),
                                                child: CachedNetworkImage(
                                                  imageUrl: post['avatar'],
                                                  placeholder: (context, url) =>
                                                      CircularProgressIndicator(
                                                    color: Colors.red,
                                                  ),
                                                  errorWidget:
                                                      (context, url, error) =>
                                                          Icon(Icons.error),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 10),
                                              child: Text(
                                                post['nama'],
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 10),
                                              child: Text(
                                                timeag,
                                                style: TextStyle(fontSize: 12),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Expanded(
                                          child: Row(
                                            //mainAxisSize: MainAxisSize.max,
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [myPopMenu(post['title'])],
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Flexible(
                                          child: Padding(
                                            /// Give name text a Padding
                                            padding: const EdgeInsets.all(10.0),
                                            child: ReadMoreText(
                                              post['title'],
                                              trimLines: 3,
                                              style: TextStyle(
                                                  color: Colors.black),
                                              colorClickableText: Colors.pink,
                                              trimMode: TrimMode.Line,
                                              trimCollapsedText:
                                                  '(Selengkapnya..)',
                                              trimExpandedText: '(Tutup)',
                                              lessStyle: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.grey),
                                              moreStyle: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.blue),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    Padding(

                                        /// Give name text a Padding
                                        padding: const EdgeInsets.all(6.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.stretch,
                                          children: [
                                            GestureDetector(
                                              // onDoubleTapDown:
                                              //     _handleDoubleTapDown,
                                              onDoubleTap: () {
                                                var lokasidata = snapshot.data
                                                    .elementAt(index);
                                                _documentId =
                                                    lokasidata['email'];
                                                print('Likes:  $_documentId');
                                                //print('GridClick');
                                              },
                                              // onDoubleTapCancel: (){
                                              //   print('GridClick');
                                              // },
                                              child: ClipRRect(
                                                borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(8.0),
                                                  topRight:
                                                      Radius.circular(8.0),
                                                ),
                                                child: CachedNetworkImage(
                                                    imageUrl:
                                                        post['image'] == null
                                                            ? Container()
                                                            : post['image'],
                                                    placeholder: (context,
                                                            url) =>
                                                        Center(
                                                          child: SizedBox(
                                                            height: 30,
                                                            width: 30,
                                                            child:
                                                                CircularProgressIndicator(
                                                              color: Colors.red,
                                                            ),
                                                          ),
                                                        ),
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            Container()),
                                              ),
                                            ),
                                          ],
                                        )),
                                    Row(
                                      // mainAxisSize: MainAxisSize.max,
                                      //mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: LikeButton(
                                            size: 28,
                                            circleColor: CircleColor(
                                                start: Color(0xff00ddff),
                                                end: Color(0xff0099cc)),
                                            bubblesColor: BubblesColor(
                                              dotPrimaryColor:
                                                  Color(0xff33b5e5),
                                              dotSecondaryColor:
                                                  Color(0xff0099cc),
                                            ),
                                            likeBuilder: (bool isLiked) {
                                              return Icon(
                                                Icons.favorite,
                                                color: isLiked
                                                    ? Colors.red
                                                    : Colors.grey,
                                                size: 28,
                                              );
                                            },
                                            likeCount:
                                                post['likes'] == null
                                                    ? 0
                                                    : int.parse(post['likes']),
                                            countBuilder: (int count,
                                                bool isLiked, String text) {
                                              var color = isLiked
                                                  ? Colors.red
                                                  : Colors.grey;
                                              Widget result;
                                              if (count == 0) {
                                                result = Text(
                                                  "Suka",
                                                  style:
                                                      TextStyle(color: color),
                                                );
                                              } else
                                                result = Text(
                                                  'Disukai',
                                                  style:
                                                      TextStyle(color: color),
                                                );
                                              return result;
                                            },
                                          ),
                                        ),
                                        // SizedBox(
                                        //   height: 40,
                                        //   width: 40,
                                        //   child: IconButton(
                                        //       onPressed: () {
                                        //         setState(() {
                                        //           isLike = true;
                                        //         });
                                        //       },
                                        //       icon:Icon(Icons.favorite_border_outlined)),
                                        // ),
                                        // Row(
                                        //   children: [
                                        //     Padding(
                                        //       padding: const EdgeInsets.only(right: 5),
                                        //       child: Text(post['likes'] == null ? '0' : post['likes']),
                                        //     ),
                                        //     Text('Disukai'),
                                        //   ],
                                        // ),
                                        // SizedBox(
                                        //   height: 40,
                                        //   width: 40,
                                        //   child: IconButton(
                                        //       onPressed: () {},
                                        //       icon: new Tab(
                                        //         icon: Image.asset(
                                        //             "assets/comment.png"),
                                        //       )),
                                        // ),
                                        // Padding(
                                        //   padding: const EdgeInsets.all(4.0),
                                        //   child: Text(
                                        //       'Disukai',
                                        //       style: TextStyle(
                                        //         fontSize: 13,
                                        //         color: Colors.grey[400],
                                        //       )),
                                        // )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      );
                    }),
              ),
              Container(
                height: 50,
                // decoration: BoxDecoration(
                //   borderRadius: BorderRadius.circular(40),
                //             color: Color(0x00000000),
                //             // boxShadow: [
                //             //   BoxShadow(color: Colors.transparent),
                //             // ],
                //           ),
                alignment: FractionalOffset.bottomCenter,
                child: GestureDetector(
                  onTap: () {
                    if (postowner == null) {
                      _onAlertPost(context);
                    } else {
                      showModalBottomSheet(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.vertical(
                                  top: Radius.circular(25.0))),
                          backgroundColor: Colors.white,
                          context: context,
                          isScrollControlled: true,
                          builder: (context) {
                            return StatefulBuilder(builder:
                                (BuildContext context, StateSetter picState) {
                              return FractionallySizedBox(
                                heightFactor: 0.7,
                                child: Wrap(
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      // mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: TextField(
                                            maxLines: null,
                                            decoration: InputDecoration(
                                                hintText:
                                                    'Apa yang anda pikirkan'),
                                            // autofocus: true,
                                            controller: _descController,
                                          ),
                                        ),
                                      ],
                                    ),
                                    GestureDetector(
                                      onTap: () async {
                                        showModalBottomSheet(
                                            context: context,
                                            builder: (BuildContext bc) {
                                              return SafeArea(
                                                child: Container(
                                                  child: new Wrap(
                                                    children: <Widget>[
                                                      new ListTile(
                                                          leading: new Icon(Icons
                                                              .photo_library),
                                                          title: new Text(
                                                              'Galeri Foto'),
                                                          onTap: () async {
                                                            FocusManager
                                                                .instance
                                                                .primaryFocus
                                                                ?.unfocus();
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                            try {
                                                              final pickedFile =
                                                                  await picker.pickImage(
                                                                      source:
                                                                          ImageSource
                                                                              .gallery,
                                                                      imageQuality:
                                                                          100,
                                                                      maxHeight:
                                                                          500,
                                                                      maxWidth:
                                                                          500);
                                                              picState(() {
                                                                if (pickedFile !=
                                                                    null) {
                                                                  _image = File(
                                                                      pickedFile
                                                                          .path);
                                                                  fileName = _image
                                                                      .path
                                                                      .split(
                                                                          '/')
                                                                      .last;
                                                                  print(
                                                                      'image selected : $_image');
                                                                  picState(() {
                                                                    isEditImage =
                                                                        true;
                                                                    imagefile =
                                                                        _image;
                                                                  });
                                                                } else {
                                                                  print(
                                                                      'No image selected.');
                                                                  return true;
                                                                }
                                                              });
                                                            } catch (e) {
                                                              print(e);
                                                            }
                                                          }),
                                                      new ListTile(
                                                        leading: new Icon(
                                                            Icons.photo_camera),
                                                        title:
                                                            new Text('Kamera'),
                                                        onTap: () async {
                                                          FocusManager.instance
                                                              .primaryFocus
                                                              ?.unfocus();
                                                          Navigator.of(context)
                                                              .pop();
                                                          if (!mounted) return;
                                                          try {
                                                            final pickedFile =
                                                                await picker.pickImage(
                                                                    source: ImageSource
                                                                        .camera,
                                                                    imageQuality:
                                                                        100,
                                                                    maxHeight:
                                                                        500,
                                                                    maxWidth:
                                                                        500);
                                                            picState(() {
                                                              if (pickedFile !=
                                                                  null) {
                                                                _image = File(
                                                                    pickedFile
                                                                        .path);
                                                                fileName =
                                                                    _image.path
                                                                        .split(
                                                                            '/')
                                                                        .last;
                                                                print(
                                                                    'image selected : $_image');
                                                                picState(() {
                                                                  isEditImage =
                                                                        true;
                                                                  imagefile =
                                                                      _image;
                                                                });
                                                              } else {
                                                                print(
                                                                    'No image selected.');
                                                                return true;
                                                              }
                                                            });
                                                          } catch (e) {
                                                            print(e);
                                                          }
                                                        },
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              );
                                            });
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          children: [
                                            Icon(Icons.add_a_photo_outlined),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 8),
                                              child: Text('Tambahkan Foto'),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    imagefile != null
                                        ? Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Container(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              height: 250,
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                  fit: BoxFit.cover,
                                                  image: FileImage(imagefile),
                                                ),
                                              ),
                                            ),
                                          )
                                        : Center(
                                            child: Image.asset(
                                                'assets/noimage.png')),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Center(
                                        child: SizedBox(
                                            width: 150,
                                            height: 50,
                                            child: ElevatedButton(
                                                onPressed: () {
                                                  DateFormat dateFormat =
                                                      DateFormat(
                                                          "yyyy-MM-dd HH:mm:ss");
                                                  var skrg = dateFormat
                                                      .format(DateTime.now());
                                                      var ids = int.parse(idmember);
                                                  Map<String, String> maps = {
                                                    "titlepost":
                                                        _descController.text,
                                                    "postdate": skrg,
                                                    "idmember": ids.toString()
                                                  };
                                                  if (!isEditImage) {
                                                    proccedRequest(
                                                        _descController.text,
                                                        int.parse(idmember));
                                                  } else {
                                                    addImage(maps, _image.path);
                                                  }
                                                  setState(() {
                                                    Navigator.pop(context);
                                                  });
                                                  print('post');
                                                  // if (_descController
                                                  //     .text.isEmpty) {
                                                  //   Toast.show(
                                                  //       'Isi Story tidak boleh kosong',
                                                  //       context,
                                                  //       duration: 2,
                                                  //       gravity: Toast.CENTER);

                                                  // } else {

                                                  // }
                                                },
                                                child: Text('Post'))),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            });
                          });
                    }
                  },
                  child: ClipRRect(
                    child: SizedBox(
                      height: 50,
                      width: 50,
                      child: Container(
                          height: 48,
                          width: 48,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(40),
                            color: Colors.grey[200],
                            boxShadow: [
                              BoxShadow(color: Colors.grey[100]),
                            ],
                          ),
                          child: Icon(Icons.add_a_photo)),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }

  Future<void> uploadFile() async {
    String fileName = path.basename(_image.path);
    await storageFB.ref('story/$fileName').putFile(_image).then((taskSnapshot) {
      print("task done");

// download url when it is uploaded
      if (taskSnapshot.state == TaskState.success) {
        storageFB.ref('story/$fileName').getDownloadURL().then((url) {
          print("Here is the URL of Image $url");
          postStory(postowner, url);
          _descController.clear();
          _image = null;
        }).catchError((onError) {
          print("Got Error $onError");
        });
      }
    });
    // String fileName = path.basename(_image.path);
    // try {
    //   await storageFB
    //       .ref('uploads/$fileName')
    //       .putFile(_image);
    // } on FirebaseException catch (e) {
    //   print(e.message);
    // }
  }

  // Future uploadImageToFirebase(BuildContext context) async {
  //   String fileName = path.basename(_image.path);
  //   var urls = storageFB.ref().child('uploads/$fileName');
  //   UploadTask uploadTask = storageFB.ref().putFile(_image);
  //   TaskSnapshot taskSnapshot = await uploadTask.whenComplete(() => {
  //         urls.getDownloadURL().then((value) => () {
  //               print('Done $value');
  //             })
  //       });
  //   print('Done $taskSnapshot');
  // }

  void postStory(String owner, String image) async {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    tgl = dateFormat.format(DateTime.now());
    //print('DataOwner ' + owner);
    var title = _descController.text;
    // var deviceEventData =
    //     "{\"content_type\":\"power_data\",\"content_value\":{\"active_power\":\"0.0\",\"current_rms\":\"0.0\",\"voltage_rms\":\"0.0\"}}";
    var isLike = 0;
    var isComment = 0;
    var imageurl = image;
    var commentUID = 'uid';
    var avatar = imageDataServer;
    var namaposter = nama;
    var isOwner = owner;
    var timepost = tgl;

    await fireStoreDB.collection("Story").doc().set({
      "owner": isOwner,
      "title": title,
      "isLike": isLike,
      "isComment": isComment,
      "imageurl": imageurl,
      "commentUID": commentUID,
      "avatar": avatar,
      "namaposter": namaposter,
      "timepost": timepost
    });
  }

  List<String> menuKategori = [
    "assets/member.png",
    "assets/rss.png",
    "assets/twocar.png",
    "assets/adduser.png",
    "assets/shop.png",
    "assets/chat.png",
  ];
  List<String> menuName = [
    "Member",
    "Story",
    "Kopdar",
    "Daftar",
    "Marketplace",
    "Grup Chat",
  ];

  List imageSlider = ["assets/"];

  _onAlertPost(context) {
    var alertStyle = AlertStyle(
      overlayColor: Colors.transparent,
      animationType: AnimationType.fromBottom,
      isCloseButton: false,
      isOverlayTapDismiss: false,
      descStyle: TextStyle(fontWeight: FontWeight.bold),
      animationDuration: Duration(milliseconds: 400),
      alertBorder: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
        side: BorderSide(
          color: Colors.grey,
        ),
      ),
      titleStyle: TextStyle(
        color: Colors.red,
      ),
    );

    Alert(
      context: context,
      style: alertStyle,
      type: AlertType.info,
      title: "MOHON MAAF",
      desc: "Anda belum terdaftar atau bukan member",
      buttons: [
        DialogButton(
          child: Text(
            "TUTUP",
            style: TextStyle(
                color: Colors.black, fontSize: 15, fontWeight: FontWeight.bold),
          ),
          onPressed: () => Navigator.pop(context),
          color: Colors.orange[400],
          radius: BorderRadius.circular(20.0),
        ),
      ],
    ).show();
  }

  // cekMenu(String email) {
  //   if (email == emailOwner) {
  //     myPopMenu();
  //     print('yang punya postingan');
  //   } else {
  //     {
  //       myPopMenu2();
  //       print('bukan yang punya postingan');
  //     }
  //   }
  //}

  Widget myPopMenu(String subjek) {
    return PopupMenuButton(
      child: Icon(
        Icons.more_vert,
        size: 25.0,
        color: Colors.black,
      ),
      itemBuilder: (BuildContext bc) => [
        // PopupMenuItem(child: Text('Ubah'), value: "Edit"),
        // PopupMenuItem(child: Text('Hapus'), value: "Delete"),
        PopupMenuItem(child: Text('Bagikan'), value: "Share"),
        // add pop menu
      ],
      onSelected: (value) {
        setState(() {
          if (value == "Edit") {
            print('Edit');
            //_onAlertButtonsPressed(context);
          } else if (value == "Delete") {
            print('Delete');
            // Navigator.of(context)
            //     .push(MaterialPageRoute(builder: (context) => About()));
          } else if (value == "Share") {
            print('share');
            Share.share('Lihat Story saya di HAIMobile \n https://play.google.com/store/apps/details?id=com.hai.mobile', subject: subjek);
            // Navigator.of(context).push(
            //     MaterialPageRoute(builder: (context) => WallSwitch1GangEU()));
          }
        });
      },
    );
  }

  // Widget myPopMenu2() {
  //   if (emailposter == emailOwner) {
  //     menuenable = true;
  //     print('yang punya postingan');
  //   } else {
  //     {
  //       menuenable = false;
  //       print('bukan yang punya postingan');
  //     }
  //   }
  //   return PopupMenuButton(
  //     child: Icon(
  //       Icons.more_vert,
  //       size: 25.0,
  //       color: Colors.black,
  //     ),
  //     itemBuilder: (BuildContext bc) => [
  //       // PopupMenuItem(child: Visibility(
  //       //   visible: menuenable,
  //       //   child: Text('Ubah')), value: "Edit"),
  //       // PopupMenuItem(child: Visibility(
  //       //   visible: menuenable,
  //       //   child: Text('Hapus')), value: "Delete"),
  //       PopupMenuItem(child: Text('Bagikan'), value: "Share"),
  //       // add pop menu
  //     ],
  //     onSelected: (value) {
  //       setState(() {
  //         if (value == "Edit") {
  //           print('Edit');
  //           //_onAlertButtonsPressed(context);
  //         } else if (value == "Delete") {
  //           print('Delete');
  //           // Navigator.of(context)
  //           //     .push(MaterialPageRoute(builder: (context) => About()));
  //         } else if (value == "Share") {
  //           print('share');
  //           // Navigator.of(context).push(
  //           //     MaterialPageRoute(builder: (context) => WallSwitch1GangEU()));
  //         }
  //       });
  //     },
  //   );
  // }

  proccedRequest(String title, int id) async {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    var skrg = dateFormat.format(DateTime.now());
    try {
      //var client = Dio(BaseOptions(baseUrl: Endpoint.BASE_URL));
      Map maps = ({"titlepost": title, "idmember": id, 'postdate': skrg});
      print('ygdikirim : $maps');
      var options = Options(
        //headers: {"Accept": "application/json"},
        //contentType: "multipart/form-data",
        followRedirects: false,
        // validateStatus: (status) {
        //   return status < 500;
        // }
      );
      Dio _dio = new Dio();
      var send = await _dio.post(Endpoint.BASE_URL + Endpoint.ADD_STORIES,
          data: maps, options: options);

      // Map<String, dynamic> imageData = {
      //         "image": await MultipartFile.fromFile(path),
      //     };

      if (send.statusCode == 200) {
        setState(() {
          Toast.show('Story berhasil diposting', context);
        });
      } else if (send.statusCode == 302) {
        setState(() {
          Toast.show('Story Gagal diposting!', context);
        });
      }
    } on DioError catch (e) {
      if (e.response.statusCode == 500) {
        setState(() {
          Toast.show('Story Gagal diposting!', context);
        });
        print('Error Response');
        Toast.show("Server ERROR", context,
            duration: Toast.LENGTH_SHORT, backgroundColor: Colors.deepOrange);
      }
      if (e.response.statusCode == 404) {
        setState(() {
          Toast.show('Not found!', context);
        });
        print('Server Error');
      }
      if (e.response.statusCode == 302) {
        setState(() {
          Toast.show('Gagal update profile!', context);
        });
        print('Server Error');
      }
      if (e.type == DioErrorType.connectTimeout) {
        print('Device Offline');
        setState(() {
          //_onDaftarGagal(context);
        });
      }
      print('Command ' + e.response.statusCode.toString());
    }
  }
}
