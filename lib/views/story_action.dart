import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mobile/model/storyModel.dart';
import 'package:native_pdf_view/native_pdf_view.dart';
import 'package:readmore/readmore.dart';

class StoryAction extends StatefulWidget {
  final StoryModel _user;
  StoryAction(this._user);
  @override
  _StoryActionState createState() => _StoryActionState();
}

class _StoryActionState extends State<StoryAction> {
  
  final _transformationController = TransformationController();
  TapDownDetails _doubleTapDetails;
  

  void _handleDoubleTap() {
    if (_transformationController.value != Matrix4.identity()) {
      _transformationController.value = Matrix4.identity();
    } else {
      final position = _doubleTapDetails.localPosition;
      // For a 3x zoom
      _transformationController.value = Matrix4.identity()
        ..translate(-position.dx * 2, -position.dy * 2)
        ..scale(3.0);
      // Fox a 2x zoom
      // ..translate(-position.dx, -position.dy)
      // ..scale(2.0);
    }
  }

  void _handleDoubleTapDown(TapDownDetails details) {
    _doubleTapDetails = details;
  }

  @override
  Widget build(BuildContext context) {
    // var ku;
    // if(_user.jabatan == 'Ketua Umum' || _user.jabatan == 'Sekretaris' || _user.jabatan == 'HUMAS'){
    //   ku = Colors.red;
    // }else{
    //   ku = Colors.blue;
    // }
    /// [InkWell] to listen to tap and give ripple effect
    return Container(
      color: Colors.white,
      child: InkWell(
        //onTap: () =>  Navigator.push(context, MaterialPageRoute(builder: (context) => MemberDetail(_user))),
        child: Container(
          /// Give nice padding
          child: Container(
            child: Card(
              semanticContainer: true,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              elevation: 4,
              margin: EdgeInsets.all(5),
              child: Column(
                children: [
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.end,
                  //   children: [
                  //     Icon(Icons.more_vert),
                  //   ],
                  // ),
                  Row(
                    //smainAxisSize: MainAxisSize.max,
                    children: [
                      Padding(
                        padding:
                            const EdgeInsets.only(top: 12, bottom: 12, left: 4),
                        child: Hero(
                          tag: "avatar_" + widget._user.id.toString(),
                          child: CircleAvatar(
                            radius: 30,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(60),
                              child: CachedNetworkImage(
                                imageUrl: widget._user.avatar,
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(
                                  color: Colors.red,
                                ),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Text(widget._user.name),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Text(widget._user.postDate),
                          ),
                        ],
                      ),
                      Expanded(
                        child: Row(
                          //mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            myPopMenu(),
                            // IconButton(
                            //   splashColor: Colors.grey[100],
                            //   splashRadius: 20,
                            //   onPressed: () {
                            //     print('more');
                            //     _showMaterialDialog(context);
                            //   },
                            //   icon: Icon(Icons.more_vert),
                            // )
                          ],
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Flexible(
                        child: Padding(
                          /// Give name text a Padding
                          padding: const EdgeInsets.all(10.0),
                          child: ReadMoreText(
                            'high-quality native (super fast) interfaces for iOS and Android apps with the unified codebase.high-quality native (super fast) interfaces for iOS and Android apps with the unified codebase.',
                            trimLines: 3,
                            style: TextStyle(color: Colors.black),
                            colorClickableText: Colors.pink,
                            trimMode: TrimMode.Line,
                            trimCollapsedText: '(Selengkapnya..)',
                            trimExpandedText: '(Tutup)',
                            lessStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                                color: Colors.grey),
                            moreStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                                color: Colors.blue),
                          ),
                        ),
                      )
                    ],
                  ),
                  Padding(
                      /// Give name text a Padding
                      padding: const EdgeInsets.all(6.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          GestureDetector(
                            onDoubleTapDown: _handleDoubleTapDown,
                            onDoubleTap: _handleDoubleTap,
                            onTap: () {
                              // PhotoView(imageProvider: NetworkImage(_user.imagePic),
                              // tightMode: true,
                              // );
                              print('GridClick');
                            },
                            child: InteractiveViewer(
                              transformationController:
                                  _transformationController,
                              child: ClipRRect(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(8.0),
                                  topRight: Radius.circular(8.0),
                                ),
                                child: CachedNetworkImage(
                                  imageUrl: widget._user.imagePic,
                                  placeholder: (context, url) =>
                                      Center(
                                        child: SizedBox(
                                          height: 30,
                                          width: 30,
                                          child:CircularProgressIndicator(
                                            color: Colors.red,
                                          ),
                                        ),
                                      ),
                                  errorWidget: (context, url, error) =>
                                      Icon(Icons.error),
                                ),
                              ),
                            ),
                          ),
                        ],
                      )),
                  Row(
                    // mainAxisSize: MainAxisSize.max,
                    //mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      SizedBox(
                        height: 40,
                        width: 40,
                        child: IconButton(
                            onPressed: () {},
                            icon: new Tab(
                              icon: Image.asset("assets/heart.png"),
                            )),
                      ),
                      SizedBox(
                        height: 40,
                        width: 40,
                        child: IconButton(
                            onPressed: () {},
                            icon: new Tab(
                              icon: Image.asset("assets/comment.png"),
                            )),
                      ),
                      SizedBox(
                          height: 40,
                          width: 40,
                          child: IconButton(
                              onPressed: () {},
                              icon: new Tab(
                                icon: Image.asset("assets/share.png"),
                              ))),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget myPopMenu() {
    return PopupMenuButton(
      child: Icon(
        Icons.more_vert,
        size: 25.0,
        color: Colors.black,
      ),
      itemBuilder: (BuildContext bc) => [
        PopupMenuItem(
            child: Text('Ubah'), value: "Edit"),
        PopupMenuItem(
            child: Text('Hapus'), value: "Delete"),
        // PopupMenuItem(
        //     child: Text(Languages.of(context).actionLogout), value: "SignOut"),

        PopupMenuItem(
            child: Text('Bagikan'), value: "Share"),
        // add pop menu
      ],
      onSelected: (value) {
        setState(() {
          if (value == "Edit") {
            print('Edit');
            //_onAlertButtonsPressed(context);
          } else if (value == "Delete") {
            print('Delete');
            // Navigator.of(context)
            //     .push(MaterialPageRoute(builder: (context) => About()));
          } else if (value == "Share") {
            print('share');
            // Navigator.of(context).push(
            //     MaterialPageRoute(builder: (context) => WallSwitch1GangEU()));
          } 
          
        });
      },
    );
  }
}

