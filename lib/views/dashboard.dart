import 'dart:convert';

import 'package:carousel_pro/carousel_pro.dart';
import 'package:dio/dio.dart';
//import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:mobile/daftar.dart';
import 'package:mobile/login.dart';
import 'package:mobile/model/push_notification.dart';
import 'package:mobile/services/endpoint.dart';
import 'package:mobile/utils/shared.dart';
import 'package:mobile/views/adart.dart';
import 'package:mobile/views/artikel.dart';
import 'package:mobile/views/bengkel.dart';
import 'package:mobile/views/chats/rooms.dart';
import 'package:mobile/views/daftar.dart';
import 'package:mobile/views/grupchat.dart';
import 'package:mobile/views/kopdar.dart';
import 'package:mobile/views/marketplace.dart';
import 'dart:math' as math;
import 'package:http/http.dart' as http;
import 'package:mobile/views/member.dart';
import 'package:mobile/model/memberModel.dart';
import 'package:mobile/views/memberall.dart';
import 'package:mobile/views/profile.dart';
import 'package:mobile/views/story.dart';
import 'package:mobile/views/tentang.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print("Handling a background message: ${message.messageId}");
}

class Dashboard extends StatefulWidget {
  String currentUser;
  String owner;
  bool loginState;
  Dashboard({this.currentUser, this.owner, this.loginState});

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  int _currentIndex = 0;
  TabController tabController;
  final _firebaseAuth = FirebaseAuth.instance;
  PushNotification _notificationInfo;
  FirebaseMessaging _messaging;
  SharedPreferences prefs;
  Response response;
  List<MemberModel> _listMember = [];

  int _totalNotifications;
  bool isDaftarStatus;
  var _currentUser;
  var _currIds;
  var _nama;
  var namaUser;
  var _idmember;
  var _image;

  Future<Null> getAllMember() async {
    var dio = Dio();
    response = await dio.get(Endpoint.BASE_URL + Endpoint.ALL_MEMBER);
    if (response.statusCode == 200) {
      //Map<String,dynamic> getdata = response.data;
      final data = response.data;
      for (Map i in data) {
        //var datas = _listMember[0].name;
        _listMember.add(MemberModel.fromJson(i));
        //print('Data $datas');
      }
    }
    //print('Datanya : ${memberModel.id}');
    //return memberModel;
  }

  /* get single member */
  Future<Null> getMemberSingle(String email) async {
    String url = Endpoint.BASE_URL + Endpoint.MEMBER_SINGLE + email;
    final response = await http.get(Uri.parse(url));
    var responseData = json.decode(response.body);

    if (response.statusCode == 200) {
      setState(() {
        _idmember = responseData[0]['id_member'];
        _nama = responseData[0]['nama'];
        _image = responseData[0]['avatar'];
        setIntoSharedPreferences(_idmember, _nama, _image);
      });
    } else {
      print('Bukan Member/Belum daftar');
    }
  }

  Future<Null> getSharedPrefs() async {
    prefs = await SharedPreferences.getInstance();
    // _currentUser = prefs.getString("currentUser");
    // _currIds = prefs.getString("uid");
    isDaftarStatus = prefs.getBool('isDaftar');
    // if (_currentUser == null) {
    //   Navigator.pushReplacement(context,
    //       MaterialPageRoute(builder: (BuildContext context) => Login()));
    //   print('Harusnya Login');
    // }
    if (isDaftarStatus == null) {
      //infoDaftar();
      Future.delayed(const Duration(seconds: 1), () async {
        //WidgetsBinding.instance.addPostFrameCallback((_) {
        infoDaftar();
      });
      //});
    }
  }

  void registerNotification() async {
    await Firebase.initializeApp();
    _messaging = FirebaseMessaging.instance;

    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

    NotificationSettings settings = await _messaging.requestPermission(
      alert: true,
      badge: true,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');

      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        print(
            'Message title: ${message.notification?.title}, body: ${message.notification?.body}, data: ${message.data}');

        // Parse the message received
        PushNotification notification = PushNotification(
          title: message.notification?.title,
          body: message.notification?.body,
          dataTitle: message.data['title'],
          dataBody: message.data['body'],
        );

        setState(() {
          _notificationInfo = notification;
          _totalNotifications++;
        });

        if (_notificationInfo != null) {
          // For displaying the notification as an overlay
          // showSimpleNotification(
          //   Text(_notificationInfo.title),
          //   leading: NotificationBadge(totalNotifications: _totalNotifications),
          //   subtitle: Text(_notificationInfo.body),
          //   background: Colors.cyan.shade700,
          //   duration: Duration(seconds: 2),
          // );
        }
      });
    } else {
      print('User declined or has not accepted permission');
    }
  }

  // For handling notification when the app is in terminated state
  checkForInitialMessage() async {
    await Firebase.initializeApp();
    RemoteMessage initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();

    if (initialMessage != null) {
      PushNotification notification = PushNotification(
        title: initialMessage.notification?.title,
        body: initialMessage.notification?.body,
        dataTitle: initialMessage.data['title'],
        dataBody: initialMessage.data['body'],
      );

      setState(() {
        _notificationInfo = notification;
        _totalNotifications++;
      });
    }
  }

  Future<String> getNama() {
    _nama = SharedPref.getNamaFromPreferences().then((value) {
      if (value == null) {
        return;
      } else {
        setState(() {
          namaUser = value;
          print('Namanya : $value');
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    getSharedPrefs();
    registerNotification();
    checkForInitialMessage();
    getNama();
    // For handling notification when the app is in background
    // but not terminated
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      PushNotification notification = PushNotification(
        title: message.notification?.title,
        body: message.notification?.body,
        dataTitle: message.data['title'],
        dataBody: message.data['body'],
      );

      setState(() {
        _notificationInfo = notification;
        _totalNotifications++;
      });
    });
    super.initState();

    // Future.delayed(const Duration(seconds: 2), () async {
    //   infoDaftar();
    // });
    //checkUser();
    //tabController = TabController(length: 3, vsync: this);
  }

  infoDaftar() {
    var alertStyle = AlertStyle(
      overlayColor: Colors.transparent,
      animationType: AnimationType.fromTop,
      isCloseButton: false,
      isOverlayTapDismiss: false,
      descStyle: TextStyle(fontWeight: FontWeight.bold),
      animationDuration: Duration(milliseconds: 400),
      alertBorder: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
        side: BorderSide(
          color: Colors.grey,
        ),
      ),
      titleStyle: TextStyle(
        color: Colors.black,
      ),
    );

    Alert(
      context: context,
      style: alertStyle,
      type: AlertType.info,
      title: "Informasi",
      desc:
          "Silahkan registrasi ulang \n Untuk kebutuhan sistem baru kami. \n\n Jika sudah registrasi,\n abaikan pesan ini!",
      buttons: [
        DialogButton(
          child: Text(
            "Nanti",
            style: TextStyle(color: Colors.black, fontSize: 15),
          ),
          onPressed: () => Navigator.pop(context),
          color: Colors.orange[400],
          radius: BorderRadius.circular(20.0),
        ),
        DialogButton(
          child: Text(
            "Registrasi",
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
          onPressed: () {
            Navigator.pop(context);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Daftar()),
              // (Route<dynamic> route) => false,
            );
          },
          color: Colors.blue,
          radius: BorderRadius.circular(20.0),
        ),
      ],
    ).show();
  }

  void listenMessage() {
    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      print("message recieved");
      print(event.notification.body);
    });
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      print('Message clicked!');
    });
  }

  void checkUser() {
    if (_firebaseAuth.currentUser == null) {
      Future.delayed(Duration.zero, () async {
        Navigator.push(context,
            MaterialPageRoute(builder: (BuildContext context) => Login()));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: new Center(
          child: Image.asset(
            'assets/logosplash.png',
            height: 80,
            width: 80,
            scale: 1.0,
          ),
        ),
      ),
      body: SafeArea(
        child: Center(
          child: Stack(
            fit: StackFit.expand,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 12, top: 6),
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 4),
                        child: ListTile(
                          // leading: CircleAvatar(
                          //     radius: 30.0,
                          //     backgroundColor: Colors.grey[400],
                          //     backgroundImage:
                          //         AssetImage('assets/noimageuser.png')),
                          // title: Text(
                          //   'HAI, ',
                          //   style: TextStyle(
                          //       fontWeight: FontWeight.bold, fontSize: 17),
                          // ),
                          subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Text('HAI,'),
                                  Text(
                                    namaUser == null ? 'User' : namaUser,
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Text('Selamat datang kembali'),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                      child: SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              bannerSlide(),
                              //Expanded(flex: 4, child: bannerSlide()),
                              Padding(
                                padding: const EdgeInsets.all(6.0),
                                child: Text(
                                  'Kategori',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 18.0,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 200,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      top: 12, bottom: 12),
                                  child: GridView.builder(
                                    physics: BouncingScrollPhysics(),
                                    itemCount: menuKategori.length,
                                    gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 5,
                                      crossAxisSpacing: 5.0,
                                      mainAxisSpacing: 5.0,
                                    ),
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return GestureDetector(
                                        onTap: () {
                                          if (index == 0) {
                                            setState(() {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        MemberAll()),
                                              );
                                              // Navigator.push(
                                              //   context,
                                              //   MaterialPageRoute(
                                              //       builder: (context) =>
                                              //           Member()),
                                              // );
                                            });
                                          } else if (index == 1) {
                                            setState(() {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        Story()),
                                              );
                                            });
                                          } else if (index == 2) {
                                            setState(() {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        Kopdar()),
                                              );
                                            });
                                          } else if (index == 3) {
                                            setState(() {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        Daftar()),
                                              );
                                            });
                                          } else if (index == 4) {
                                            setState(() {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        Marketplace()),
                                              );
                                              // Navigator.push(
                                              //   context,
                                              //   MaterialPageRoute(
                                              //       builder: (context) => Profile()),
                                              // );
                                            });
                                          } else if (index == 5) {
                                            setState(() {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        Artikel()),
                                              );
                                            });
                                          } else if (index == 6) {
                                            setState(() {
                                              // Navigator.push(
                                              //   context,
                                              //   MaterialPageRoute(
                                              //       builder: (context) =>
                                              //           GrupChat()),
                                              // );
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        Bengkel()),
                                              );
                                            });
                                          } else if (index == 7) {
                                            setState(() {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        AdArt()),
                                              );
                                            });
                                          } else if (index == 8) {
                                            setState(() {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        Tentang()),
                                              );
                                            });
                                          }
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              top: 9, left: 6, right: 6),
                                          child: Container(
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                              color: Colors.grey[200],
                                              boxShadow: [
                                                BoxShadow(
                                                    color: Colors.grey[100]),
                                              ],
                                            ),
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Column(
                                                children: [
                                                  SizedBox(
                                                    height: 25,
                                                    width: 25,
                                                    child: Image.asset(
                                                        menuKategori[index]),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 4),
                                                    child: Text(menuName[index],
                                                        style: TextStyle(
                                                            fontSize: 9,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold)),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),

                                        // Card(
                                        //   //color: Color.fromRGBO(0, 162, 233, 120),
                                        //   child:

                                        // ),
                                      );
                                    },
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(6.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Sponsor',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                        fontSize: 18.0,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 70, // constrain height
                                      child: bannerSponsor(),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )))
                ],
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        // backgroundColor: colorScheme.surface,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.grey,
        // selectedLabelStyle: textTheme.caption,
        // unselectedLabelStyle: textTheme.caption,
        onTap: (value) {
          // Respond to item press.
          setState(() => _currentIndex = value);
          if (_currentIndex == 0) {
            print('Home');
          } else if (_currentIndex == 1) {
            print('notif');
          } else {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Profile()),
            );
          }
        },
        items: [
          BottomNavigationBarItem(
            label: 'Beranda',
            icon: Icon(Icons.home),
          ),
          BottomNavigationBarItem(
            label: 'Notifikasi',
            icon: Icon(Icons.notifications),
          ),
          BottomNavigationBarItem(
            label: 'Profil',
            icon: Icon(Icons.person),
          ),
          // BottomNavigationBarItem(
          //   title: Text('News'),
          //   icon: Icon(Icons.library_books),
          // ),
        ],
      ),
    );
  }

  bannerSponsor() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.grey[200],
        boxShadow: [
          BoxShadow(color: Colors.grey[100]),
        ],
      ),
      child: SizedBox(
          height: 70,
          child: Carousel(
            images: [
              AssetImage('assets/banner1.png'),
              AssetImage('assets/banner2.png'),
            ],
            dotSize: 3.0,
            //dotSpacing: 15.0,
            autoplayDuration: Duration(seconds: 10),
            //animationCurve: Curves.fastLinearToSlowEaseIn,
            //animationDuration: Duration(seconds: 1),
            dotColor: Colors.transparent,
            indicatorBgPadding: 5.0,
            moveIndicatorFromBottom: 180.0,
            dotBgColor: Colors.transparent,
            borderRadius: true,
          )),
    );
  }
  // loadMember(){
  //   return Expanded(
  //     child: ListView(
  //     /// iterate [UserModel] through _userlist
  //       children: _userlist.map((user) => Member(user)).toList(),
  //     ),
  //   );
  // }

  bannerSlide() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: const EdgeInsets.all(3.0),
            child: Row(
              children: [
                Text('Events Terbaru',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0)),
              ],
            ),
          ),
          Container(
            child: SizedBox(
                height: 180,
                child: Carousel(
                  images: [
                    AssetImage('assets/mastercamping.png'),
                    AssetImage('assets/logosplash.png'),
                  ],
                  dotSize: 4.0,
                  dotSpacing: 15.0,
                  autoplayDuration: Duration(seconds: 10),
                  //animationCurve: Curves.fastLinearToSlowEaseIn,
                  //animationDuration: Duration(seconds: 1),
                  dotColor: Colors.grey,
                  indicatorBgPadding: 5.0,
                  moveIndicatorFromBottom: 180.0,
                  dotBgColor: Colors.transparent,
                  borderRadius: true,
                )),
          ),
        ],
      ),
    );
  }

  kategoriMenu() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Text(
                  'Kategori',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 22.0),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void setIntoSharedPreferences(String id, String nama, String avatar) async {
    prefs = await SharedPreferences.getInstance();
    await prefs.setString("idmember", id);
    await prefs.setString('nama', nama);
    await prefs.setString("avatar", avatar);
  }

  List<String> menuKategori = [
    "assets/member.png",
    "assets/rss.png",
    "assets/twocar.png",
    "assets/adduser.png",
    "assets/shop.png",
    "assets/article.png",
    "assets/car-repair.png",
    // "assets/chat.png",
    "assets/docs.png",
    "assets/info.png",
  ];
  List<String> menuName = [
    "Member",
    "Story",
    "Kopdar",
    "Daftar",
    "FJB",
    'Artikel',
    'Bengkel',
    // "Chat",
    "AD/ART",
    "Tentang",
  ];
  static List<String> links = [
    "https://i.pinimg.com/originals/cc/18/8c/cc188c604e58cffd36e1d183c7198d21.jpg",
    "https://www.kyoceradocumentsolutions.be/blog/wp-content/uploads/2019/03/iStock-881331810.jpg",
    "https://resources.matcha-jp.com/resize/720x2000/2020/04/23-101958.jpeg"
  ];

  List imageSlider = [
    "https://i.pinimg.com/originals/bb/61/8d/bb618db7d825e2e03c8d86f781a65e06.jpg",
    "assets/logosplash.png",
  ];
}

class NotificationBadge extends StatelessWidget {
  final int totalNotifications;

  const NotificationBadge({this.totalNotifications});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 40.0,
      height: 40.0,
      decoration: new BoxDecoration(
        color: Colors.red,
        shape: BoxShape.circle,
      ),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            '$totalNotifications',
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        ),
      ),
    );
  }
}
