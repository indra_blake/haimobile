import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:mobile/services/endpoint.dart';
import 'dart:math' as math;

import 'package:mobile/views/dashboard.dart';
import 'package:mobile/views/memberaction.dart';
import 'package:mobile/model/memberModel.dart';
import 'package:mobile/views/memberall.dart';
import 'package:mobile/views/memberdetail2.dart';

class Member extends StatefulWidget {
  // List memberList;
  // Member({this.memberList});
  @override
  _MemberState createState() => _MemberState();
}

class _MemberState extends State<Member> {
  bool _enabled = true;
  Response response;
  List<MemberModel> _listMember = [];
  static int page = 0;
  ScrollController _sc = new ScrollController();
  bool isLoading = false;
  var datanya;
  List users = new List();
  MemberModel _user;
  final List<MemberModel> _userlist = [
    MemberModel('1', "Rizky HMP", "https://i.ibb.co/sK3BH5H/Screenshot-1.png",
        "Ketua Umum", "#004", "Avega 2010", "Bekasi", "30-Nov-2020"),
    MemberModel("2", "Terma WR", "https://i.ibb.co/n0XQwpk/Screenshot-2.png",
        "Sekretaris", "#005", "Accent X3 2003", "Tangerang", "30-Nov-2020"),
    MemberModel("3", "Deden NS", "https://i.ibb.co/2kTMS6h/Screenshot-3.png",
        "HUMAS", "#009", "Verna 2002", "Tangerang", "06-Jun-2020"),
    MemberModel(
        "4",
        "IndraMbullzz",
        "http://hyundaiaccentindonesia.com/mobile/public/avatar/abul.jpg",
        "Bendahara",
        "#002",
        "New Accent",
        "Tangerang",
        "16-Mar-2021"),
  ];
  List<MemberModel> parseMember(String responseBody) {
    print('member $responseBody');
    // final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

    // return parsed
    //     .map<MemberModel>((json) => MemberModel.fromJson(json))
    //     .toList();
  }

  Future<Null> getAllMember() async {
    isLoading = true;
    print('ambilmember');
    var dio = Dio();
    response = await dio.get(Endpoint.BASE_URL + Endpoint.ALL_MEMBER2);
    if (response.statusCode == 200) {
      //Map<String,dynamic> getdata = response.data;
      final data = response.data;
      //final getData = data['original'];

      for (Map i in data) {
        //var datas = _listMember[0].name;
        setState(() {
          _listMember.add(MemberModel.fromJson(i));
          isLoading = false;
        });
      }
    }
    //print('Datanya : ${memberModel.id}');
    //return memberModel;
  }

  @override
  void initState() {
    // datanya = widget.memberList;
    // //print('Isi List ${datanya.nama}');
    // for (var d in datanya) {
    //   print('Data==>>> $d');
    // }
    // if (datanya]) {
    //   print('Duplicate');
    // } else {
    //   this._getMoreData(page);
    // }
    getAllMember();
    super.initState();
    // _sc.addListener(() {
    //   if (_sc.position.pixels == _sc.position.maxScrollExtent) {
    //     _getMoreData(page);
    //   }
    // });
  }

  // void _getMoreData(int index) async {
  //   if (!isLoading) {
  //     setState(() {
  //       isLoading = true;
  //     });
  //     List tList = [];
  //     for (int i = 0; i < widget.memberList.length; i++) {
  //       _listMember.add(widget.memberList[i]);
  //       print('Dataaaa=> $tList');
  //     }

  //     setState(() {
  //       isLoading = false;
  //       //users.addAll(tList);
  //       print('Data==>> $users');
  //       page++;
  //     });
  //   }
  // }

  Widget _buildProgressIndicator() {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Center(
        child: SizedBox(
            height: 10,
            width: 10,
            child: CircularProgressIndicator(
              color: Colors.red,
            ),
          ),
      ),
    );
  }

  Widget _buildList() {
    return ListView.builder(
      itemCount: _listMember.length,
      // Add one more item for progress indicator
      padding: EdgeInsets.symmetric(vertical: 8.0),
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: const EdgeInsets.all(6.0),
          child: GestureDetector(
            onTap: () {
              var gy = _listMember[index].id;
              print('Index $gy');
              setState(() {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MemberDetail2(
                            avatars: _listMember[index].avatar,
                            chapters: _listMember[index].chapter,
                            jabatans: _listMember[index].jabatan,
                            namas: _listMember[index].name,
                            nopungs: _listMember[index].nopung,
                            regdate: _listMember[index].joinDate,
                            units: _listMember[index].unit,
                          )),
                );
              });
            },
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  child: ListTile(
                    leading: CircleAvatar(
                      radius: 30.0,
                      backgroundImage: _listMember[index].avatar == null
                          ? AssetImage('assets/noimageuser.png')
                          : NetworkImage(
                              _listMember[index].avatar,
                            ),
                    ),
                    title: Text(
                      (_listMember[index].name),
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                    ),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          (_listMember[index].nopung == null
                              ? 'Belum Ada Nopung'
                              : _listMember[index].nopung),
                          style: TextStyle(
                              fontSize: 13, fontWeight: FontWeight.w400),
                        ),
                        Text((_listMember[index].chapter)),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
      controller: _sc,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(
            Icons.arrow_back_sharp,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        title: new Center(
          child: Image.asset(
            'assets/logosplash.png',
            height: 80,
            width: 80,
            scale: 1.0,
          ),
        ),
        actions: <Widget>[
          Container(
              alignment: Alignment.centerRight,
              child: Visibility(visible: true, child: Text('HAImobile'))),
        ],
      ),
      body: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(6.0),
            child: Text('List Member',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 22.0)),
          ),
          Center(
            child: Text('Pengurus Pusat HAI',
                style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                    fontSize: 17.0)),
          ),
          Container(
            height: 360,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: GridView.builder(
                itemCount: _userlist.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 5.0,
                  mainAxisSpacing: 5.0,
                ),
                itemBuilder: (BuildContext context, int index) {
                  var ku;
                  if (_userlist[index].jabatan == 'Ketua Umum' ||
                      _userlist[index].jabatan == 'Sekretaris' ||
                      _userlist[index].jabatan == 'HUMAS' ||
                      _userlist[index].jabatan == 'Bendahara') {
                    ku = Colors.red;
                  } else {
                    ku = Colors.blue;
                  }
                  return GestureDetector(
                    onTap: () {
                      setState(() {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MemberDetail2(
                                    avatars: _userlist[index].avatar,
                                    chapters: _userlist[index].chapter,
                                    jabatans: _userlist[index].jabatan,
                                    namas: _userlist[index].name,
                                    nopungs: _userlist[index].nopung,
                                    regdate: _userlist[index].joinDate,
                                    units: _userlist[index].unit,
                                  )),
                        );
                      });
                    },
                    child: Card(
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      //color: Colors.grey,
                      child: Column(
                        children: [
                          Container(
                            height: 120,
                            width: MediaQuery.of(context).size.width * 0.90,
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.transparent),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                            child: FittedBox(
                              child: ClipRRect(
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(40),
                                    topLeft: Radius.circular(40)),
                                child: CachedNetworkImage(
                                  imageUrl: _userlist[index].avatar,
                                  placeholder: (context, url) =>
                                      _buildProgressIndicator(),
                                  errorWidget: (context, url, error) => Center(
                                      child: SizedBox(
                                          height: 28,
                                          width: 28,
                                          child: Image.asset(
                                              "assets/noimageuser.png"))),
                                ),
                              ),
                              fit: BoxFit.fill,
                            ),
                          ),
                          Divider(
                            height: 5,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8),
                            child: Column(
                              //crossAxisAlignment: CrossAxisAlignment.start,
                              //mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Row(
                                  //mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    Text(
                                      _userlist[index].name,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Text(_userlist[index].jabatan,
                                        style: TextStyle(
                                            color:
                                                _userlist[index].jabatan != null
                                                    ? ku
                                                    : Colors.blue)),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 4, left: 4),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Member',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 22.0)),
                Padding(
                  padding: const EdgeInsets.only(right: 6.0),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => MemberAll()),
                      );
                      print('lihat semua');
                    },
                    child: Text('Lihat semua',
                        style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.normal,
                            fontSize: 15.0)),
                  ),
                ),
              ],
            ),
          ),
          new Expanded(
              flex: 6,
              child: isLoading
                  ? Center(
                      child: SizedBox(
                        height: 30,
                        width: 30,
                        child: CircularProgressIndicator(
                          color: Colors.red,
                        ),
                      ),
                    )
                  : _buildList()),
        ],
      ),
      // bottomNavigationBar: BottomNavigationBar(
      //   currentIndex: 0, // this will be set when a new tab is tapped
      //   items: [
      //     BottomNavigationBarItem(
      //       icon: IconButton(
      //         icon: Icon(Icons.home),
      //         iconSize: 40,
      //         onPressed: () {
      //           Navigator.push(
      //             context,
      //             MaterialPageRoute(builder: (context) => Dashboard()),
      //           );
      //         },
      //       ),
      //       title: new Text('Home'),
      //     ),
      //     BottomNavigationBarItem(
      //       icon: IconButton(
      //         icon: Image.asset('assets/send.png'),
      //         iconSize: 40,
      //         onPressed: () {},
      //       ),
      //       title: new Text('Story'),
      //     ),
      //     BottomNavigationBarItem(
      //         icon: Icon(Icons.person), title: Text('Profile'))
      //   ],
      // ),
    );
  }

  bannerSlide() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: const EdgeInsets.all(3.0),
            child: Row(
              children: [
                Text('Member List',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 22.0)),
              ],
            ),
          ),
          ListView.builder(
            itemCount: menuName.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: Text('${menuName[index]}'),
              );
            },
          ),
        ],
      ),
    );
  }

  List<String> menuKategori = [
    "assets/member.png",
    "assets/rss.png",
    "assets/twocar.png",
    "assets/adduser.png",
    "assets/shop.png",
    "assets/chat.png",
  ];
  List<String> menuName = [
    "Member",
    "Story",
    "Kopdar",
    "Daftar",
    "Marketplace",
    "Grup Chat",
  ];

  List imageSlider = ["assets/"];
}
