import 'package:flutter/material.dart';
import 'package:native_pdf_view/native_pdf_view.dart';

class AdArt extends StatefulWidget {
  @override
  _AdArtState createState() => _AdArtState();
}

class _AdArtState extends State<AdArt> {
  static final int _initialPage = 2;
  int _actualPageNumber = _initialPage, _allPagesCount = 0;
  bool isSampleDoc = true;
  PdfController _pdfController;

  @override
  void initState() {
    _pdfController = PdfController(
      document: PdfDocument.openAsset('assets/adart.pdf'),
      initialPage: _initialPage,
    );
    super.initState();
  }

  @override
  void dispose() {
    _pdfController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: new IconButton(
            icon: new Icon(
              Icons.arrow_back_sharp,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          backgroundColor: Colors.white,
          elevation: 0,
          title: Center(
            child: Image.asset(
            'assets/logosplash.png',
            height: 80,
            width: 80,
            scale: 1.0,
          ),
          ),
          actions: <Widget>[
            Container(
              alignment: Alignment.center,
              child: Visibility(
                visible: true,
                child: Text('HAIMobile')
              )
            ),
            // IconButton(
            //   icon: Icon(Icons.navigate_next),
            //   onPressed: (){

            //   },
            // ),
            // IconButton(
            //   icon: Icon(Icons.refresh),
            //   onPressed: () {
            //     // if (isSampleDoc) {
            //     //   _pdfController.loadDocument(
            //     //       PdfDocument.openAsset('assets/dummy.pdf'));
            //     // } else {
            //     //   _pdfController.loadDocument(
            //     //       PdfDocument.openAsset('assets/sample.pdf'));
            //     // }
            //     // isSampleDoc = !isSampleDoc;
            //   },
            // )
          ],
        ),
        body: SafeArea(
          child: Container(
            child: Column(
              children: [
                titlePage(),
                Expanded(
                  child: PdfView(
                    documentLoader: Center(child: CircularProgressIndicator()),
                    pageLoader: Center(child: CircularProgressIndicator()),
                    controller: _pdfController,
                    onDocumentLoaded: (document) {
                      setState(() {
                        _allPagesCount = document.pagesCount;
                      });
                    },
                    onPageChanged: (page) {
                      setState(() {
                        _actualPageNumber = page;
                      });
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      );
  }

  titlePage() {
    return Padding(
      padding: const EdgeInsets.only(top: 5, left: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('AD/ART',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0)),
        ],
      ),
    );
  }
}
