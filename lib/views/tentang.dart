import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:mobile/login.dart';
import 'dart:math' as math;

import 'package:mobile/views/dashboard.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Tentang extends StatefulWidget {
  @override
  _TentangState createState() => _TentangState();
}

class _TentangState extends State<Tentang> {
  final _firebaseAuth = FirebaseAuth.instance;
  SharedPreferences prefs;
  bool lockInBackground = true;
  bool notificationsEnabled = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(
            Icons.arrow_back_sharp,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        title: Center(
          child: Image.asset(
            'assets/logosplash.png',
            height: 80,
            width: 80,
            scale: 1.0,
          ),
        ),
        actions: <Widget>[
          Container(
              alignment: Alignment.center,
              child: Visibility(visible: true, child: Text('HAImobile'))),
        ],
      ),
      body: SafeArea(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              child: SettingsList(
                backgroundColor: Colors.white,
                contentPadding: EdgeInsets.only(top: 8, left: 5),
                sections: [
                  SettingsSection(
                    title: 'Common',
                    tiles: [
                      SettingsTile(
                        title: 'Bahasa',
                        subtitle: 'Indonesian',
                        leading: Icon(Icons.language),
                        onPressed: (context) {
                          // Navigator.of(context).push(MaterialPageRoute(
                          //   builder: (_) => LanguagesScreen(),
                          // ));
                        },
                      ),
                      // CustomTile(
                      //   child: Container(
                      //     color: Color(0xFFEFEFF4),
                      //     padding: EdgeInsetsDirectional.only(
                      //       start: 14,
                      //       top: 12,
                      //       bottom: 30,
                      //       end: 14,
                      //     ),
                      //     child: Text(
                      //       'You can setup the language you want',
                      //       style: TextStyle(
                      //         color: Colors.grey.shade700,
                      //         fontWeight: FontWeight.w400,
                      //         fontSize: 13.5,
                      //         letterSpacing: -0.5,
                      //       ),
                      //     ),
                      //   ),
                      // ),
                      SettingsTile(
                        title: 'Environment',
                        subtitle: 'HAIMobile Beta version',
                        leading: Icon(Icons.cloud_queue),
                      ),
                    ],
                  ),
                  SettingsSection(
                    title: 'Akun',
                    tiles: [
                      SettingsTile(
                        title: 'Bagikan Aplikasi',
                        leading: Icon(Icons.share),
                        onPressed: (context) {
                          Share.share('Download sekarang, Aplikasi Pengguna Hyundai Accent \n https://play.google.com/store/apps/details?id=com.hai.mobile', subject: 'HAIMobile Aplikasi Klub Mobil Hyundai Accent Indonesia');
                        },
                      ),
                      SettingsTile(
                        title: 'Keluar Aplikasi',
                        leading: Icon(Icons.exit_to_app),
                        onPressed: (context) {
                          _onAlertButtonsPressed(context);
                        },
                      ),
                    ],
                  ),
                  // SettingsSection(
                  //   title: 'Security',
                  //   tiles: [
                  //     SettingsTile.switchTile(
                  //       title: 'Lock app in background',
                  //       leading: Icon(Icons.phonelink_lock),
                  //       switchValue: lockInBackground,
                  //       onToggle: (bool value) {
                  //         setState(() {
                  //           lockInBackground = value;
                  //           notificationsEnabled = value;
                  //         });
                  //       },
                  //     ),
                  //     SettingsTile.switchTile(
                  //       title: 'Use fingerprint',
                  //       subtitle: 'Allow application to access stored fingerprint IDs.',
                  //       leading: Icon(Icons.fingerprint),
                  //       onToggle: (bool value) {},
                  //       switchValue: false,
                  //     ),
                  //     SettingsTile.switchTile(
                  //       title: 'Change password',
                  //       leading: Icon(Icons.lock),
                  //       switchValue: true,
                  //       onToggle: (bool value) {},
                  //     ),
                  //     SettingsTile.switchTile(
                  //       title: 'Enable Notifications',
                  //       enabled: notificationsEnabled,
                  //       leading: Icon(Icons.notifications_active),
                  //       switchValue: true,
                  //       onToggle: (value) {},
                  //     ),
                  //   ],
                  // ),
                  SettingsSection(
                    title: 'Lain-lain',
                    tiles: [
                      SettingsTile(
                          title: 'Syarat & Ketentuan ',
                          leading: Icon(Icons.description)),
                      SettingsTile(
                          title: 'Open source licenses',
                          leading: Icon(Icons.collections_bookmark)),
                    ],
                  ),
                  CustomSection(
                    child: Column(
                      children: [
                        // Padding(
                        //   padding: const EdgeInsets.only(top: 22, bottom: 8),
                        //   child: Image.asset(
                        //     'assets/settings.png',
                        //     height: 50,
                        //     width: 50,
                        //     color: Color(0xFF777777),
                        //   ),
                        // ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Version: 1.0.0',
                            style: TextStyle(color: Color(0xFF777777)),
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Created By: IndraBlake @ 2021',
                                    style: TextStyle(color: Color(0xFF777777)),
                                  ),
                                ),
                                Text(
                                  'Contact: dediindrajaya@gmail.com',
                                  style: TextStyle(color: Color(0xFF777777)),
                                ),
                              ],
                            )),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      // bottomNavigationBar: BottomNavigationBar(
      //   currentIndex: 0, // this will be set when a new tab is tapped
      //   items: [
      //     BottomNavigationBarItem(
      //       icon: new Icon(Icons.home),
      //       title: new Text('Home'),
      //     ),
      //     BottomNavigationBarItem(
      //       icon: IconButton(
      //         icon: Image.asset('assets/send.png'),
      //         iconSize: 40,
      //         onPressed: () {},
      //       ),
      //       title: new Text('Story'),
      //     ),
      //     BottomNavigationBarItem(
      //         icon: Icon(Icons.person), title: Text('Profile'))
      //   ],
      // ),
    );
  }

  _onAlertButtonsPressed(context) {
    Alert(
      context: context,
      type: AlertType.warning,
      title: "INFORMASI",
      desc: "Jika anda keluar dari HAIMobile, anda harus login kembali!!",
      buttons: [
        DialogButton(
          child: Text(
            "Batal",
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          color: Color.fromRGBO(0, 179, 134, 1.0),
        ),
        DialogButton(
          color: Colors.red,
          child: Text(
            "Keluar",
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          onPressed: () {
            Navigator.pop(context);
            _firebaseAuth.signOut();
            removeLoginState();
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => Login()),
              (Route<dynamic> route) => false,
            );
          },
          // gradient: LinearGradient(colors: [
          //   Color.fromRGBO(116, 116, 191, 1.0),
          //   Color.fromRGBO(52, 138, 199, 1.0),
          // ]),
        )
      ],
    ).show();
  }

  removeLoginState() async {
    prefs = await SharedPreferences.getInstance();
    await prefs.remove('currentUser');
    await prefs.remove('email');
    await prefs.remove('uid');
  
  }
}
