import 'dart:io';
import 'dart:ui';

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mobile/model/memberModel.dart';

class MemberDetail2 extends StatefulWidget {
  var avatars;
  var namas;
  var nopungs;
  var regdate;
  var chapters;
  var units;
  var jabatans;
  MemberDetail2(
      {this.avatars,
      this.namas,
      this.nopungs,
      this.regdate,
      this.chapters,
      this.units,
      this.jabatans});
  //const MemberDetail2({ Key key }) : super(key: key);

  @override
  _MemberDetail2State createState() => _MemberDetail2State();
}

class _MemberDetail2State extends State<MemberDetail2> {
  List listMem = [];
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey();

  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add(myInterceptor);
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    super.dispose();
  }

  bool myInterceptor(bool stopDefaultButtonEvent, RouteInfo info) {
    if (!mounted) return false;
    print("BACK BUTTON!"); // Do some stuff.
    Navigator.pop(context);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return Scaffold(
      key: scaffoldState,
      body: Stack(
        children: <Widget>[
          _buildWidgetAlbumCover(mediaQuery),
          _buildWidgetActionAppBar(mediaQuery),
          _buildWidgetArtistName(mediaQuery),
          _buildWidgetFloatingActionButton(mediaQuery),
          _buildWidgetListSong(mediaQuery),
        ],
      ),
    );
  }

  Widget _buildWidgetArtistName(MediaQueryData mediaQuery) {
    return SizedBox(
      height: mediaQuery.size.height / 1.8,
      child: Padding(
        padding: const EdgeInsets.only(left: 20.0),
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return Stack(
              children: <Widget>[
                Positioned(
                  child: Text(
                    widget.namas,
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: "CoralPen",
                      fontSize: 30.0,
                    ),
                  ),
                  top: constraints.maxHeight - 60.0,
                ),
                // Positioned(
                //   child: Text(
                //     "Ariana",
                //     style: TextStyle(
                //       color: Colors.white,
                //       fontFamily: "CoralPen",
                //       fontSize: 72.0,
                //     ),
                //   ),
                //   top: constraints.maxHeight - 140.0,
                // ),
                // Positioned(
                //   child: Text(
                //     "Tranding",
                //     style: TextStyle(
                //       color: Color(0xFF7D9AFF),
                //       fontSize: 14.0,
                //       fontFamily: "Campton_Light",
                //       fontWeight: FontWeight.w800,
                //     ),
                //   ),
                //   top: constraints.maxHeight - 160.0,
                // ),
              ],
            );
          },
        ),
      ),
    );
  }

  Widget _buildWidgetListSong(MediaQueryData mediaQuery) {
    return Padding(
      padding: EdgeInsets.only(
        left: 20.0,
        top: mediaQuery.size.height / 1.8 + 48.0,
        right: 20.0,
        bottom: mediaQuery.padding.bottom + 16.0,
      ),
      child: Column(
        children: <Widget>[
          _buildWidgetHeaderSong(),
          SizedBox(height: 16.0),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 8),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            'Nopung',
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontFamily: "Campton_Light",
                              fontSize: 15,
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Text(
                          widget.nopungs == null
                              ? 'Belum Ada Nopung'
                              : widget.nopungs,
                          //widget._user.nopung,
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 15,
                          ),
                        ),
                        SizedBox(width: 24.0),
                        // Icon(
                        //   Icons.more_horiz,
                        //   color: Colors.grey,
                        // ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 8),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            'Chapter',
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontFamily: "Campton_Light",
                              fontSize: 15,
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Text(
                          widget.chapters,
                          //widget._user.chapter,
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 15,
                          ),
                        ),
                        SizedBox(width: 24.0),
                        // Icon(
                        //   Icons.more_horiz,
                        //   color: Colors.grey,
                        // ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 8),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            'Unit',
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontFamily: "Campton_Light",
                              fontSize: 15,
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Text(
                          widget.units,
                          //widget._user.unit,
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 15,
                          ),
                        ),
                        SizedBox(width: 24.0),
                        // Icon(
                        //   Icons.more_horiz,
                        //   color: Colors.grey,
                        // ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 8),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            'Bergabung',
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontFamily: "Campton_Light",
                              fontSize: 15,
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Text(
                          widget.regdate,
                          //widget._user.joinDate,
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 15,
                          ),
                        ),
                        SizedBox(width: 24.0),
                        // Icon(
                        //   Icons.more_horiz,
                        //   color: Colors.grey,
                        // ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 8),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            'Status',
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontFamily: "Campton_Light",
                              fontSize: 15,
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Text(
                          widget.jabatans,
                          //widget._user.jabatan,
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 15,
                          ),
                        ),
                        SizedBox(width: 24.0),
                        // Icon(
                        //   Icons.more_horiz,
                        //   color: Colors.grey,
                        // ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildWidgetHeaderSong() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              "Detail Member",
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w600,
                fontSize: 24.0,
                fontFamily: "Campton_Light",
              ),
            ),

            // Text(
            //   "Show all",
            //   style: TextStyle(
            //     color: Color(0xFF7D9AFF),
            //     fontWeight: FontWeight.w600,
            //     fontFamily: "Campton_Light",
            //   ),
            // ),
          ],
        ),
        Divider(
          height: 8,
          color: Colors.grey,
        )
      ],
    );
  }

  Widget _buildWidgetFloatingActionButton(MediaQueryData mediaQuery) {
    Icon kuy;
    if (widget.jabatans == 'Ketua Umum' ||
        widget.jabatans == 'Sekretaris' ||
        widget.jabatans == 'HUMAS') {
      kuy = Icon(Icons.admin_panel_settings_rounded);
    } else {
      kuy = Icon(Icons.verified);
    }
    return Align(
      alignment: Alignment.topRight,
      child: Padding(
        padding: EdgeInsets.only(
          top: mediaQuery.size.height / 1.8 - 32.0,
          right: 32.0,
        ),
        child: FloatingActionButton(
          child: kuy,
          backgroundColor: Colors.blue,
          onPressed: () {
            //_navigatorToMusicPlayerScreen(listSong[0].title);
          },
        ),
      ),
    );
  }

  // void _navigatorToMusicPlayerScreen(String title) {
  //   Navigator.of(scaffoldState.currentContext)
  //       .push(MaterialPageRoute(builder: (context) {
  //     return MusicPlayerScreen(title);
  //   }));
  // }

  Widget _buildWidgetActionAppBar(MediaQueryData mediaQuery) {
    return Padding(
      padding: EdgeInsets.only(
        left: 16.0,
        top: mediaQuery.padding.top + 16.0,
        right: 16.0,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildWidgetAlbumCover(MediaQueryData mediaQuery) {
    return Container(
      width: double.infinity,
      height: mediaQuery.size.height / 1.8,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(48.0),
        ),
        image: DecorationImage(
          image: CachedNetworkImageProvider(
            widget.avatars
            ),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}

class Song {
  String title;
  String duration;

  Song({this.title, this.duration});

  @override
  String toString() {
    return 'Song{title: $title, duration: $duration}';
  }
}
