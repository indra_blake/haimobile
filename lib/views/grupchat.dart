import 'package:flutter/material.dart';
import 'dart:math' as math;

import 'package:mobile/views/dashboard.dart';

class GrupChat extends StatefulWidget {
  @override
  _GrupChatState createState() => _GrupChatState();
}

class _GrupChatState extends State<GrupChat> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          leading: new IconButton(
            icon: new Icon(
              Icons.arrow_back_sharp,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          backgroundColor: Colors.white,
          elevation: 0,
          title: Center(
            child: Image.asset(
              'assets/logosplash.png',
              height: 80,
              width: 80,
              scale: 1.0,
            ),
          ),
          actions: <Widget>[
            Container(
                alignment: Alignment.center,
                child: Visibility(visible: true, child: Text('HAImobile'))),
          ]),
      body: SafeArea(
        child: Center(
          child: Image.asset(
            'assets/coomingsoon.jpg',
            height: 160,
            width: 160,
            scale: 1.0,
          ),
        ),
      ),
      // bottomNavigationBar: BottomNavigationBar(
      //   currentIndex: 0, // this will be set when a new tab is tapped
      //   items: [
      //     BottomNavigationBarItem(
      //       icon: new Icon(Icons.home),
      //       title: new Text('Home'),
      //     ),
      //     BottomNavigationBarItem(
      //       icon: IconButton(
      //         icon: Image.asset('assets/send.png'),
      //         iconSize: 40,
      //         onPressed: () {},
      //       ),
      //       title: new Text('Story'),
      //     ),
      //     BottomNavigationBarItem(
      //         icon: Icon(Icons.person), title: Text('Profile'))
      //   ],
      // ),
    );
  }

  List<String> menuKategori = [
    "assets/member.png",
    "assets/rss.png",
    "assets/twocar.png",
    "assets/adduser.png",
    "assets/shop.png",
    "assets/chat.png",
  ];
  List<String> menuName = [
    "Member",
    "Story",
    "Kopdar",
    "Daftar",
    "Marketplace",
    "Grup Chat",
  ];

  List imageSlider = ["assets/"];
}
