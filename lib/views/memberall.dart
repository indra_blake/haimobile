import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:easy_loader/easy_loader.dart';
import 'package:flutter/material.dart';
import 'package:mobile/model/memberModel.dart';
import 'package:mobile/services/endpoint.dart';
import 'package:mobile/views/memberdetail2.dart';

class MemberAll extends StatefulWidget {
  const MemberAll({Key key}) : super(key: key);

  @override
  _MemberAllState createState() => _MemberAllState();
}

class _MemberAllState extends State<MemberAll> {
  final List<MemberModel> _userlist = [];
  bool isLoading = false;
  Response response;

  Future<Null> getAllMember() async {
    isLoading = true;
    print('ambilmember');
    var dio = Dio();
    response = await dio.get(Endpoint.BASE_URL + Endpoint.ALL_MEMBER);
    if (response.statusCode == 200) {
      //Map<String,dynamic> getdata = response.data;
      final data = response.data;
      //final getData = data['original'];

      for (Map i in data) {
        //var datas = _listMember[0].name;
        setState(() {
          _userlist.add(MemberModel.fromJson(i));
          isLoading = false;
        });
      }
    }
  }

  @override
  void initState() {
    getAllMember();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(
            Icons.arrow_back_sharp,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        title: new Center(
          child: Image.asset(
            'assets/logosplash.png',
            height: 80,
            width: 80,
            scale: 1.0,
          ),
        ),
        actions: <Widget>[
          Container(
              alignment: Alignment.centerRight,
              child: Visibility(visible: true, child: Text('HAImobile'))),
        ],
      ),
      body: Stack(
        children: [
          Container(
            child: GridView.builder(
              itemCount: _userlist.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 5.0,
                mainAxisSpacing: 5.0,
              ),
              itemBuilder: (BuildContext context, int index) {
                var ku;
                if (_userlist[index].jabatan == 'Ketua Umum' ||
                    _userlist[index].jabatan == 'Sekretaris' ||
                    _userlist[index].jabatan == 'HUMAS' ||
                    _userlist[index].jabatan == 'Bendahara') {
                  ku = Colors.red;
                } else {
                  ku = Colors.blue;
                }
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MemberDetail2(
                                  avatars: _userlist[index].avatar,
                                  chapters: _userlist[index].chapter,
                                  jabatans: _userlist[index].jabatan,
                                  namas: _userlist[index].name,
                                  nopungs: _userlist[index].nopung,
                                  regdate: _userlist[index].joinDate,
                                  units: _userlist[index].unit,
                                )),
                      );
                    });
                  },
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0))),
                    //color: Colors.grey,
                    child: Column(
                      children: [
                        Container(
                          height: 120,
                          width: MediaQuery.of(context).size.width * 0.90,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.transparent),
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(20),
                                  topLeft: Radius.circular(20))),
                          child: ClipRRect(
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(13),
                                  topLeft: Radius.circular(13)),
                              child: CachedNetworkImage(
                                imageUrl: _userlist[index].avatar,
                                fit: BoxFit.cover,
                                placeholder: (context, url) => Center(
                                        child: SizedBox(
                                          height: 30,
                                          width: 30,
                                          child:CircularProgressIndicator(
                                            color: Colors.red,
                                          ),
                                        ),
                                      ),
                                errorWidget: (context, url, error) => Center(
                                    child: SizedBox(
                                        height: 28,
                                        width: 28,
                                        child: Image.asset(
                                            "assets/noimageuser.png"))),
                              ),
                            ),
                        ),
                        Divider(
                          height: 5,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8),
                          child: Column(
                            //crossAxisAlignment: CrossAxisAlignment.start,
                            //mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Row(
                                children: [
                                 Text(
                                      _userlist[index].name,
                                       overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                ],
                              ),
                              
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(_userlist[index].jabatan,
                                      style: TextStyle(
                                          color:
                                              _userlist[index].jabatan != null
                                                  ? ku
                                                  : Colors.blue)),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 4),
                                    child: Text(
                                      _userlist[index].nopung == null
                                          ? 'Belum Ada nopung'
                                          : _userlist[index].nopung,
                                      style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ],
                              ),
                              
                            ],
                          ),
                        ),
                        
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
          isLoading
              ? EasyLoader(
                  image: AssetImage(
                    'assets/logosplash.png',
                  ),
                  iconColor: Colors.white,
                )
              : Container()
        ],
      ),
    );
  }
}
