

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobile/model/artikelModel.dart';

class ArtikelDetail extends StatefulWidget {
  
  ArtikelDetail({ Key key, this.artikelModel }) : super(key: key);
  final ArtikelModel artikelModel;
  @override
  _ArtikelDetailState createState() => _ArtikelDetailState();
}

class _ArtikelDetailState extends State<ArtikelDetail> {

  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add(myInterceptor);
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    super.dispose();
  }
  bool myInterceptor(bool stopDefaultButtonEvent, RouteInfo info) {
    print("BACK BUTTON!"); // Do some stuff.
    Navigator.pop(context);
    return true;
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        // Do something here
        Navigator.pop(context);
        print("After clicking the Android Back Button");
        return false;
     },
      child: Scaffold(
        appBar: AppBar(
          //title: Text(widget.news.title, style: GoogleFonts.lato(),),
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          leading: GestureDetector(
            onTap: () => Navigator.pop(context),
              child: Icon(Icons.close, color: Colors.grey,)),
        ),
        body: SingleChildScrollView(
          child: Card(
          elevation: 2,
            child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  children: [
                    Image.network('https://i0.wp.com/rimbakita.com/wp-content/uploads/2019/07/mata-angin.png'),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      widget.artikelModel.title,
                      style: GoogleFonts.lato(fontSize: 21, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.radio,
                              size: 20,
                              color: Colors.grey.shade500,
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Text(
                              widget.artikelModel.name == null ? 'name': widget.artikelModel.name,
                              style: GoogleFonts.lato(
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold,
                                  fontStyle: FontStyle.italic),
                              maxLines: 2,
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.calendar_today,
                              color: Colors.grey.shade500,
                              size: 20,
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Text(
                              widget.artikelModel.publishedAt.split("T")[0],
                              style: GoogleFonts.lato(
                                  fontSize: 12, fontStyle: FontStyle.italic),
                              maxLines: 2,
                            )
                          ],
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    widget.artikelModel.description != null
                        ? Text(widget.artikelModel.description,
                            style: GoogleFonts.lato(
                                fontSize: 16, fontStyle: FontStyle.normal))
                        : Container(),
                  ],
                ))),
        ),
        ),
    );
  }
}