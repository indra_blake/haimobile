import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobile/model/artikelModel.dart';
import 'package:mobile/views/artikeldetail.dart';
import 'package:http/http.dart' as http;

class Artikel extends StatefulWidget {
  const Artikel({Key key}) : super(key: key);

  @override
  _ArtikelState createState() => _ArtikelState();
}

class _ArtikelState extends State<Artikel> {
  List<ArtikelModel> newsHeadlines = [];
  bool loading = true;

  @override
  void initState() {
    super.initState();
    fetchNews();
  }

  Future fetchNews() async {
    var newsAPI =
        "https://newsapi.org/v2/everything?q=tesla&from=2021-11-16&sortBy=publishedAt&apiKey=2c56609a48094f4a8c4a19d8c2f14bf8";
    Uri myUri = Uri.parse(newsAPI);
    var response = await http.get(myUri);
    if (response.statusCode == 200) {
      var newsHeadlines = json.decode(response.body);
      List articles = newsHeadlines["articles"];
      articles.forEach((article) {
        article = ArtikelModel.fromJson(article);
        setState(() {
          this.newsHeadlines.add(article);
        });
      });
    } else {
      setState(() {
        loading = false;
        return showError();
      });
    }

    setState(() {
      loading = false;
    });
    return this.newsHeadlines;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(
            Icons.arrow_back_sharp,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        title: Center(
          child: Image.asset(
            'assets/logosplash.png',
            height: 80,
            width: 80,
          ),
        ),
        actions: <Widget>[
          Container(
              alignment: Alignment.center,
              child: Visibility(visible: true, child: Text('HAImobile'))),
        ],
      ),
      body: SafeArea(
        child: Center(
          child: Image.asset(
            'assets/coomingsoon.jpg',
            height: 160,
            width: 160,
            scale: 1.0,
          ),
        ),
      ),
      // SafeArea(
      //   child: Center(
      //     child: bodyArtikel(),
      //   ),
      // ),
      //   bottomNavigationBar: BottomNavigationBar(
      //   currentIndex: 0, // this will be set when a new tab is tapped
      //   items: [
      //     BottomNavigationBarItem(
      //       icon: new Icon(Icons.home),
      //       title: new Text('Home'),
      //     ),
      //     BottomNavigationBarItem(
      //       icon: IconButton(
      //         icon: Image.asset('assets/send.png'),
      //         iconSize: 40,
      //         onPressed: () {},
      //       ),
      //       title: new Text('Story'),
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.person),
      //       title: Text('Profile')
      //     )
      //   ],
      // ),
    );
  }

  String getCurrentDate() {
    var now = new DateTime.now();
    return "${now.day}-${now.month}-${now.year}";
  }

  bodyArtikel() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Container(
        padding: EdgeInsets.all(10),
        child: Padding(
          padding: const EdgeInsets.all(6.0),
          child: Text(
            'Artikel Detail',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.black,
              fontSize: 22.0,
            ),
          ),
        ),
      ),
      // SizedBox(
      //   height: 80,
      //   child: Container(
      //     padding: EdgeInsets.all(10),
      //     child: ListView.builder(
      //         scrollDirection: Axis.horizontal,
      //         itemCount: 4,
      //         itemBuilder: (context, index) {
      //           return Row(
      //             children: [
      //               Container(
      //                 padding: EdgeInsets.all(10),
      //                 height: 60,
      //                 width: 60,
      //                 decoration: BoxDecoration(
      //                     color: Colors.amber, shape: BoxShape.circle),
      //                 child: Center(
      //                     child: Text(
      //                   "$index",
      //                   style: GoogleFonts.lato(
      //                       fontSize: 25,
      //                       color: Colors.white,
      //                       fontWeight: FontWeight.bold),
      //                 )),
      //               ),
      //               SizedBox(
      //                 width: 10,
      //               )
      //             ],
      //           );
      //         }),
      //   ),
      // ),
      loading
          ? Center(
              child: CircularProgressIndicator(
              backgroundColor: Colors.lightBlue,
              strokeWidth: 3,
            ))
          : Expanded(
              child: Container(
                  child: ListView.builder(
                      itemCount: newsHeadlines.length,
                      itemBuilder: (context, index) {
                        return Card(
                            child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  children: [
                                    ListTile(
                                      leading: CircleAvatar(
                                        radius: 30.0,
                                        backgroundImage: newsHeadlines[index]
                                                    .urlToImage ==
                                                null
                                            ? NetworkImage(
                                                'https://via.placeholder.com/150')
                                            : NetworkImage(newsHeadlines[index]
                                                .urlToImage),
                                        backgroundColor: Colors.transparent,
                                      ),
                                      title: Text(
                                        newsHeadlines[index].title,
                                        maxLines: 1,
                                        style: GoogleFonts.lato(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      subtitle: Column(
                                        children: [
                                          newsHeadlines[index].description !=
                                                  null
                                              ? Text(
                                                  newsHeadlines[index]
                                                      .description,
                                                  style: GoogleFonts.lato(
                                                      fontSize: 12,
                                                      fontStyle:
                                                          FontStyle.italic),
                                                  maxLines: 3)
                                              : Container(),
                                        ],
                                      ),
                                      //contentPadding: EdgeInsets.all(4),

                                      onTap: () => {
                                        print('Datanya' +
                                            newsHeadlines[index].author),
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ArtikelDetail(
                                                    artikelModel:
                                                        newsHeadlines[index],
                                                  )),
                                        )
                                      },
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Row(
                                          children: [
                                            Icon(
                                              Icons.radio,
                                              size: 20,
                                              color: Colors.grey.shade500,
                                            ),
                                            SizedBox(
                                              width: 8,
                                            ),
                                            Text(
                                              newsHeadlines[index].name == null
                                                  ? 'name'
                                                  : newsHeadlines[index].name,
                                              style: GoogleFonts.lato(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold,
                                                  fontStyle: FontStyle.italic),
                                              maxLines: 2,
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Row(
                                          children: [
                                            Icon(
                                              Icons.calendar_today,
                                              color: Colors.grey.shade500,
                                              size: 20,
                                            ),
                                            SizedBox(
                                              width: 8,
                                            ),
                                            Text(
                                              newsHeadlines[index]
                                                  .publishedAt
                                                  .split("T")[0],
                                              style: GoogleFonts.lato(
                                                  fontSize: 12,
                                                  fontStyle: FontStyle.italic),
                                              maxLines: 2,
                                            )
                                          ],
                                        )
                                      ],
                                    )
                                  ],
                                )));
                      })),
            ),
    ]);
  }

  List<String> menuKategori = [
    "assets/member.png",
    "assets/rss.png",
    "assets/twocar.png",
    "assets/adduser.png",
    "assets/shop.png",
    "assets/chat.png",
  ];
  List<String> menuName = [
    "Member",
    "Story",
    "Kopdar",
    "Daftar",
    "Marketplace",
    "Grup Chat",
  ];

  List imageSlider = ["assets/"];
}

Widget showError() {
  return Container(
    child: Center(
      child: Text(
        "Not News Found, Hope world is safe",
        style: TextStyle(
            fontStyle: FontStyle.italic,
            fontWeight: FontWeight.bold,
            fontSize: 18),
      ),
    ),
  );
}
