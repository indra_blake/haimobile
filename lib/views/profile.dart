import 'dart:convert';
import 'dart:io';

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:easy_loader/easy_loader.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:images_picker/images_picker.dart';
import 'package:mobile/database/database_helper.dart';
import 'package:mobile/login.dart';
import 'package:mobile/services/endpoint.dart';
import 'package:mobile/utils/shared.dart';
import 'package:mobile/views/stikerpage.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:http/http.dart' as http;
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> with SingleTickerProviderStateMixin {
  final _firebaseAuth = FirebaseAuth.instance;
  final dbHelper = DatabaseHelper.instance;
  Response response;
  SharedPreferences prefs;
  bool showPassword = false;
  bool isLoginrun = false;
  bool isMember = false;
  List _listMember = [];
  var _namaData;
  var currentEmail;
  var _unitData;
  var _nopungData;
  var _nopungdata;
  var emailOK;
  var statusmember;
  TextEditingController _namaController = TextEditingController();
  TextEditingController _unitController = TextEditingController();
  TextEditingController _nopungController = TextEditingController();
  String path;
  String fileName;
  String imageData;
  String nama;
  String nopung;
  String unit;
  String imageDataServer;
  File _image;
  bool isEditImage = false;
  final picker = ImagePicker();
  AnimationController _animationController;
  Image imageFromPreferences;
  var _currentUser;

  Future getImage() async {
    try {
      final pickedFile = await picker.pickImage(
          source: ImageSource.gallery,
          imageQuality: 100,
          maxHeight: 500,
          maxWidth: 500);
      setState(() {
        if (pickedFile != null) {
          _image = File(pickedFile.path);
          fileName = _image.path.split('/').last;
          print('image selected : $_image');
        } else {
          print('No image selected.');
          return true;
        }
      });
    } catch (e) {
      print(e);
    }
  }

  Future<bool> addImage(Map<String, String> body, String filepath) async {
    print('Coba Jalanin ini');
    String addimageUrl = Endpoint.BASE_URL + Endpoint.EDIT_MEMBER + emailOK;
    Map<String, String> headers = {
      'Content-Type': 'multipart/form-data',
    };
    var request = http.MultipartRequest('POST', Uri.parse(addimageUrl))
      ..fields.addAll(body)
      ..headers.addAll(headers)
      ..files.add(await http.MultipartFile.fromPath('image', filepath));
    var response = await request.send();
    if (response.statusCode == 200) {
      //_onDaftarSukses(context);
      Toast.show('Profile berhasil di update', context);
      setState(() {
        isLoginrun = false;
      });
      return true;
    } else {
      //_onDaftarGagal(context);
      Toast.show('Gagal update profile!', context);
      setState(() {
        isLoginrun = false;
      });
      return false;
    }
  }

  Future<bool> addNoImage(
      String nama, String nopung, String unit, String email) async {
    String addimageUrl = Endpoint.BASE_URL + Endpoint.EDIT_MEMBER + email;
    print('Coba Jalanin ini $addimageUrl');
    var url = Uri.parse(addimageUrl);
    var response = await http.post(url,
        body: {'nama': '$nama', 'nopung': '$nopung', 'tipe_mobil': '$unit'});
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');


    if (response.statusCode == 200) {
      //_onDaftarSukses(context);
      Toast.show('Profile berhasil di update', context);
      setState(() {
        isLoginrun = false;
      });
      return true;
    } else {
      //_onDaftarGagal(context);
      Toast.show('Gagal update profile!', context);
      setState(() {
        isLoginrun = false;
      });
      return false;
    }
  }

  Future<Null> getSharedPrefs() async {
    prefs = await SharedPreferences.getInstance();
    currentEmail = prefs.getString("email");
    _currentUser = prefs.getString("currentUser");
    // isMember = prefs.getBool('isDaftar');
    // print('Bukan Member/Belum daftar $isMember');
    if (_currentUser == null || currentEmail == null) {
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (BuildContext context) => Login()));
      print('Harusnya Login');
    } else {
      setState(() {
        emailOK = currentEmail;
        getMemberSingle(emailOK);
      });
    }

    print('emailnya: $currentEmail');
  }

  Future<Null> getMemberSingle(String email) async {
    String url = Endpoint.BASE_URL + Endpoint.MEMBER_SINGLE + email;
    final response = await http.get(Uri.parse(url));
    var responseData = json.decode(response.body);

    if (response.statusCode == 200) {
      setState(() {
        nama = responseData[0]['nama'];
        nopung = responseData[0]['nopung'];
        unit = responseData[0]['tipe_mobil'];
        imageDataServer = responseData[0]['avatar'];
        statusmember = responseData[0]['status_member'];
      });
    } else {
      print('Bukan Member/Belum daftar');
    }
  }

  @override
  void initState() {
    getSharedPrefs();
    checkPermission();
    //checkDataUser();
    _animationController =
        new AnimationController(vsync: this, duration: Duration(seconds: 1));
    _animationController.repeat(reverse: true);
    super.initState();
    BackButtonInterceptor.add(myInterceptor);
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    _animationController.dispose();
    super.dispose();
  }

  bool myInterceptor(bool stopDefaultButtonEvent, RouteInfo info) {
    if (!mounted) return false;
    print("BACK BUTTON!"); // Do some stuff.
    Navigator.pop(context);
    return true;
  }

  checkPermission() async {
    var status = await Permission.photos.status;

    if (status.isDenied) {
      // We didn't ask for permission yet.
    }

    // You can can also directly ask the permission about its status.
    if (await Permission.location.isRestricted) {
      // The OS restricts access, for example because of parental controls.
    }
  }

  // checkDataUser() async {
  //   final allRows = await dbHelper.queryAllRows();
  //   allRows.forEach((data) {
  //     setState(() {
  //       path = data['imagepath'];
  //       _unitController.text = data['unit'];
  //       _namaController.text = data['nama'];
  //       _nopungController.text = data['nopung'];
  //     });
  //   });
  //   print('Hasil Query All Rows $path');
  // }

  @override
  Widget build(BuildContext context) {
    // if(path == null){
    //   path = AssetImage('graphics/background.png');
    // }
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Center(
          child: Image.asset(
            'assets/logosplash.png',
            height: 80,
            width: 80,
            scale: 1.0,
          ),
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.settings,
              color: Colors.white,
            ),
            onPressed: () {
              // Navigator.of(context).push(MaterialPageRoute(
              //     builder: (BuildContext context) => SettingsPage()));
            },
          ),
        ],
      ),
      body: Stack(
        children: [
          Container(
            padding: EdgeInsets.only(left: 16, top: 25, right: 16),
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: ListView(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Edit Profil",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                      GestureDetector(
                        onTap: (){
                          _onAlertButtonsPressed(context);
                        },
                        child: Icon(Icons.logout,color: Colors.red,),
                      ),
                      
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Center(
                    child: Stack(
                      children: [
                        GestureDetector(
                          onTap: () async {
                            setState(() {
                              isEditImage = true;
                            });
                            getImage();
                          },
                          child: Container(
                            width: 130,
                            height: 130,
                            decoration: BoxDecoration(
                                border: Border.all(
                                    width: 4,
                                    color: Theme.of(context)
                                        .scaffoldBackgroundColor),
                                boxShadow: [
                                  BoxShadow(
                                      spreadRadius: 2,
                                      blurRadius: 10,
                                      color: Colors.black.withOpacity(0.1),
                                      offset: Offset(0, 10))
                                ],
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: _image == null
                                      ? CachedNetworkImageProvider(
                                          imageDataServer,
                                          errorListener: () => AssetImage(
                                              'assets/noimageuser.png'),
                                        )
                                      : FileImage(_image),
                                  fit: BoxFit.cover,
                                )),
                          ),
                        ),
                        Positioned(
                            bottom: 0,
                            right: 0,
                            child: Container(
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                  width: 4,
                                  color:
                                      Theme.of(context).scaffoldBackgroundColor,
                                ),
                                color: Colors.blue,
                              ),
                              child: GestureDetector(
                                onTap: () async {
                                  setState(() {
                                    isEditImage = true;
                                  });
                                  getImage();
                                  // List<Media> res = await ImagesPicker.pick(
                                  //   count: 1,
                                  //   pickType: PickType.image,
                                  //   language: Language.System,
                                  //   //maxTime: 30,
                                  //   // maxSize: 500,
                                  //   // cropOpt: CropOption(
                                  //   //   aspectRatio: CropAspectRatio.wh16x9,
                                  //   // ),
                                  // );
                                  // print(res);
                                  // if (res != null) {
                                  //   print(res.map((e) => e.path).toList());
                                  //   setState(() {
                                  //     path = res[0].thumbPath;
                                  //   });
                                  //   // bool status = await ImagesPicker.saveImageToAlbum(File(res[0]?.path));
                                  //   // print(status);
                                  // }
                                },
                                child: Icon(
                                  Icons.edit,
                                  color: Colors.white,
                                ),
                              ),
                            )),
                      ],
                    ),
                  ),
                  Center(
                      child: FadeTransition(
                    opacity: _animationController,
                    child: TextButton(
                      child: Text(
                        'Lihat Cara Pemasangan Stiker',
                        style: TextStyle(fontSize: 17),
                      ),
                      onPressed: () {
                        setState(() {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => StikerPage()),
                          );
                        });
                      },
                    ),
                  )),
                  SizedBox(
                    height: 15,
                  ),
                  buildTextFieldNama(),
                  buildTextFieldNopung(),
                  //buildTextField("Unit", "********", true),
                  buildTextFieldUnit(),
                  SizedBox(
                    height: 35,
                  ),
                  ConstrainedBox(
                      constraints:
                          BoxConstraints.tightFor(width: 300, height: 50),
                      child: ElevatedButton(
                        onPressed: () {
                          print('Simpan');
                          Map<String, String> maps = {
                            "nama": _namaController.text.isEmpty
                                ? nama
                                : _namaController.text,
                            "nopung": _nopungController.text.isEmpty
                                ? nopung
                                : _nopungController.text,
                            "tipe_mobil": _unitController.text.isEmpty
                                ? unit
                                : _unitController.text,
                          };
                          if (!isEditImage) {
                            proccedRequest(
                                _namaController.text,
                                _unitController.text,
                                _nopungController.text,
                                emailOK);
                            setState(() {
                              isLoginrun = true;
                              // updateUser(
                              //     _namaController.text,
                              //     _unitController.text,
                              //     _nopungController.text,
                              //     imageDataServer);
                            });
                            // addNoImage(
                            //     _namaController.text,
                            //     _nopungController.text,
                            //     _unitController.text,
                            //     emailOK);
                          } else {
                            setState(() {
                              isLoginrun = true;
                              // updateUser(
                              //     _namaController.text,
                              //     _unitController.text,
                              //     _nopungController.text,
                              //     fileName);
                            });
                            SharedPref.saveNamaToPreferences(
                                _namaController.text);
                            addImage(maps, _image.path);
                          }
                          // if(isEditImage){

                          // }else{

                          // }
                        },
                        style: ButtonStyle(
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: BorderSide(color: Colors.blue))),
                        ),
                        child: Text(
                          "SIMPAN",
                          style: TextStyle(
                              fontSize: 14,
                              letterSpacing: 2.2,
                              color: Colors.white),
                        ),
                      )),
                ],
              ),
            ),
          ),
          isLoginrun
              ? EasyLoader(
                  image: AssetImage(
                    'assets/logosplash.png',
                  ),
                  iconColor: Colors.white,
                )
              : Container()
        ],
      ),
    );
  }

  proccedRequest(
      String nama, String tipemobil, String nopung, String email) async {
    //callLoading();
    try {
      //var client = Dio(BaseOptions(baseUrl: Endpoint.BASE_URL));
      Map maps = ({"nama": nama, "nopung": nopung, "tipe_mobil": tipemobil});
      print('ygdikirim : $maps');
      var options = Options(
        //headers: {"Accept": "application/json"},
        //contentType: "multipart/form-data",
        followRedirects: false,
        // validateStatus: (status) {
        //   return status < 500;
        // }
      );
      Dio _dio = new Dio();
      var send = await _dio.post(
          Endpoint.BASE_URL + Endpoint.EDIT_MEMBER_NO_IMAGE + email,
          data: maps,
          options: options);

      // Map<String, dynamic> imageData = {
      //         "image": await MultipartFile.fromFile(path),
      //     };

      if (send.statusCode == 200) {
        setState(() {
          Toast.show('Profile berhasil di update', context);
          isLoginrun = false;
          SharedPref.saveNamaToPreferences(nama);
        });
      } else if (send.statusCode == 302) {
        setState(() {
          Toast.show('Gagal update profile!', context);
          isLoginrun = false;
        });
      }
    } on DioError catch (e) {
      if (e.response.statusCode == 500) {
        setState(() {
          isLoginrun = false;
          Toast.show('Gagal update profile!', context);
        });
        print('Error Response');
        Toast.show("Server ERROR", context,
            duration: Toast.LENGTH_SHORT, backgroundColor: Colors.deepOrange);
      }
      if (e.response.statusCode == 404) {
        setState(() {
          isLoginrun = false;
          Toast.show('Gagal update profile!', context);
        });
        print('Server Error');
      }
      if (e.response.statusCode == 302) {
        setState(() {
          isLoginrun = false;
          Toast.show('Gagal update profile!', context);
        });
        print('Server Error');
      }
      if (e.type == DioErrorType.connectTimeout) {
        print('Device Offline');
        setState(() {
          isLoginrun = false;
          //_onDaftarGagal(context);
        });
      }
      print('Command ' + e.response.statusCode.toString());
    }
  }

  Widget buildTextFieldNama() {
    if (_namaController.text.isEmpty) {
      setState(() {
        _namaController.text = nama;
      });
    } else {
      setState(() {
        _namaController.text = _namaData;
      });
    }
    return Padding(
      padding: const EdgeInsets.only(bottom: 35.0),
      child: TextField(
        style: TextStyle(
            fontSize: 18.0,
            height: 2.0,
            color: Colors.black,
            fontWeight: FontWeight.bold),
        controller: _namaController,
        textCapitalization: TextCapitalization.sentences,
        //obscureText: isPasswordTextField ? showPassword : false,
        decoration: InputDecoration(

            // suffixIcon: isPasswordTextField
            //     ? IconButton(
            //         onPressed: () {
            //           setState(() {
            //             showPassword = !showPassword;
            //           });
            //         },
            //         icon: Icon(
            //           Icons.remove_red_eye,
            //           color: Colors.grey,
            //         ),
            //       )
            //     : null,
            contentPadding: EdgeInsets.only(bottom: 3),
            labelText: 'Nama',
            //floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: 'Nama',
            hintStyle: TextStyle(
              fontSize: 16,
              color: Colors.black54,
            )),
      ),
    );
  }

  Widget buildTextFieldNopung() {
    if (_nopungController.text.isEmpty) {
      setState(() {
        _nopungController.text = nopung;
      });
    } else {
      setState(() {
        _nopungController.text = _nopungdata;
      });
    }
    return Padding(
      padding: const EdgeInsets.only(bottom: 35.0),
      child: TextField(
        style: TextStyle(
            fontSize: 18.0,
            height: 2.0,
            color: Colors.black,
            fontWeight: FontWeight.bold),
        controller: _nopungController,
        inputFormatters: [
          LengthLimitingTextInputFormatter(4),
        ],
        //obscureText: isPasswordTextField ? showPassword : false,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.only(bottom: 3),
            labelText: 'No Punggung',
            //floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: 'No punggung',
            hintStyle: TextStyle(
              fontSize: 16,
              //fontWeight: FontWeight.bold,
              color: Colors.black,
            )),
      ),
    );
  }

  Widget buildTextFieldUnit() {
    if (_unitController.text.isEmpty) {
      setState(() {
        _unitController.text = unit;
      });
    } else {
      setState(() {
        _unitController.text = _unitData;
      });
    }
    return Padding(
      padding: const EdgeInsets.only(bottom: 35.0),
      child: TextField(
        style: TextStyle(
            fontSize: 18.0,
            height: 2.0,
            color: Colors.black,
            fontWeight: FontWeight.bold),
        controller: _unitController,
        textCapitalization: TextCapitalization.sentences,
        //obscureText: isPasswordTextField ? showPassword : false,
        decoration: InputDecoration(
            // suffixIcon: isPasswordTextField
            //     ? IconButton(
            //         onPressed: () {
            //           setState(() {
            //             showPassword = !showPassword;
            //           });
            //         },
            //         icon: Icon(
            //           Icons.remove_red_eye,
            //           color: Colors.grey,
            //         ),
            //       )
            //     : null,
            contentPadding: EdgeInsets.only(bottom: 3),
            labelText: 'Unit',
            //floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: 'Unit',
            hintStyle: TextStyle(
              fontSize: 16,
              //fontWeight: FontWeight.bold,
              color: Colors.black54,
            )),
      ),
    );
  }

  // void saveUser(String path, String nama, String unit) async {
  //   Map<String, dynamic> row = {
  //     DatabaseHelper.columnImagePath: path,
  //     DatabaseHelper.columnNama: nama,
  //     DatabaseHelper.columnUnit: unit,
  //   };
  //   final id = await dbHelper.insertUser(row);
  //   // setState(() {
  //   //   idTimer = id;
  //   // });
  //   print('ID Sqlite :' + id.toString());
  //   print('Datanya :' + row.toString());
  //   setState(() {
  //     isLoginrun = false;
  //   });
  // }

  // void updateUser(nama, unit, nopung, path) async {
  //   if (nama == null) {
  //     nama = nama;
  //   } else if (unit == null) {
  //     unit = unit;
  //   } else if (nopung == null) {
  //     nopung = nopung;
  //   }
  //   // else if(path == null){
  //   //   path = 'assets/noimageuser.png';
  //   // }
  //   final id = await dbHelper.updateUser(nama, unit, nopung, path);
  //   print('ID $id');
  //   setState(() {
  //     isLoginrun = false;
  //   });
  // }

  _onAlertButtonsPressed(context) {
    Alert(
      context: context,
      type: AlertType.warning,
      title: "INFORMASI",
      desc: "Jika anda keluar dari HAIMobile, anda harus login kembali!!",
      buttons: [
        DialogButton(
          child: Text(
            "Batal",
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          color: Color.fromRGBO(0, 179, 134, 1.0),
        ),
        DialogButton(
          color: Colors.red,
          child: Text(
            "Keluar",
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          onPressed: () {
            Navigator.pop(context);
            _firebaseAuth.signOut();
            removeLoginState();
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => Login()),
              (Route<dynamic> route) => false,
            );
          },
          // gradient: LinearGradient(colors: [
          //   Color.fromRGBO(116, 116, 191, 1.0),
          //   Color.fromRGBO(52, 138, 199, 1.0),
          // ]),
        )
      ],
    ).show();
  }

  removeLoginState() async {
    prefs = await SharedPreferences.getInstance();
    await prefs.remove('currentUser');
    await prefs.remove('email');
    await prefs.remove('uid');
  }
}
