import 'package:badges/badges.dart';
import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

import 'package:mobile/views/dashboard.dart';
import 'package:mobile/model/kopdarModel.dart';

class Kopdar extends StatefulWidget {
  @override
  _KopdarState createState() => _KopdarState();
}

class _KopdarState extends State<Kopdar> {
  final GlobalKey<ExpansionTileCardState> cardA = new GlobalKey();
  final GlobalKey<ExpansionTileCardState> cardB = new GlobalKey();
  int kopdar = 10;

  @override
  Widget build(BuildContext context) {
    var dat;
    var dat2;
    var dat3;
    final ButtonStyle flatButtonStyle = TextButton.styleFrom(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
      ),
    );
    return Scaffold(
        appBar: AppBar(
          leading: new IconButton(
            icon: new Icon(
              Icons.arrow_back_sharp,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          backgroundColor: Colors.white,
          elevation: 0,
          title: Center(
            child: Image.asset(
              'assets/logosplash.png',
              height: 80,
              width: 80,
              scale: 1.0,
            ),
          ),
          actions: <Widget>[
            Container(
              alignment: Alignment.centerRight,
              child: Visibility(
                visible: true,
                child: Text('HAImobile')
              )
            ),
            // IconButton(
            //   icon: Icon(Icons.navigate_next),
            //   onPressed: (){
                
            //   },
            // ),
          ],
        ),
        body: SafeArea(
          child: Container(
            color: Colors.grey[200],
            child: Column(
              children: [
                titlePage(),
                Expanded(
                  child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ListView.builder(
                        itemCount: titles.length,
                        itemBuilder: (BuildContext context, index) {
                          print('indexnya: $index');
                          // for (var data in _kopdarlist) {
                          //   dat = data.titleKopdar;
                          //   dat2 = data.subTitle;
                          //   dat3 = data.isi;
                          //   print("Datanya : $dat, $dat2, $dat3");
                          // }
                          return ExpansionTileCard(
                            //key: cardA,

                            leading: CircleAvatar(child: Text('K')),
                            title: Text(titles[index]),
                            subtitle: Text(subtitles[index]),

                            trailing: Text('19:30'),
                            //animateTrailing: true,
                            children: <Widget>[
                              Divider(
                                thickness: 1.0,
                                height: 1.0,
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0,
                                    vertical: 8.0,
                                  ),
                                  child: Text(
                                    isi[index],
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText2
                                        .copyWith(fontSize: 16),
                                  ),
                                ),
                              ),
                              ButtonBar(
                                alignment: MainAxisAlignment.spaceAround,
                                buttonHeight: 52.0,
                                buttonMinWidth: 90.0,
                                children: <Widget>[
                                  Column(
                                    children: [
                                      Text('Status'),
                                      Badge(
                                        toAnimate: true,
                                        animationType: BadgeAnimationType.scale,
                                        animationDuration:
                                            Duration(milliseconds: 800),
                                        shape: BadgeShape.square,
                                        badgeColor: Colors.green,
                                        borderRadius: BorderRadius.circular(8),
                                        badgeContent: Text('Segera',
                                            style:
                                                TextStyle(color: Colors.white)),
                                      ),
                                    ],
                                  ),
                                  TextButton(
                                    style: flatButtonStyle,
                                    onPressed: () {
                                      cardB.currentState?.expand();
                                    },
                                    child: Column(
                                      children: <Widget>[
                                        Icon(Icons.thumb_up),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 2.0),
                                        ),
                                        Text('Suka'),
                                      ],
                                    ),
                                  ),
                                  TextButton(
                                    style: flatButtonStyle,
                                    onPressed: () {
                                      cardB.currentState?.collapse();
                                    },
                                    child: Column(
                                      children: <Widget>[
                                        Icon(Icons.car_repair),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 2.0),
                                        ),
                                        Text('Datang'),
                                      ],
                                    ),
                                  ),
                                  TextButton(
                                    style: flatButtonStyle,
                                    onPressed: () {
                                      cardB.currentState?.toggleExpansion();
                                    },
                                    child: Column(
                                      children: <Widget>[
                                        Icon(Icons.skip_next),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 2.0),
                                        ),
                                        Text('Skip'),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          );
                        },
                      )),
                )
              ],
            ),
          ),
        ),
        // bottomNavigationBar: BottomNavigationBar(
        //   currentIndex: 0, // this will be set when a new tab is tapped
        //   items: [
        //     BottomNavigationBarItem(
        //       icon: new Icon(Icons.home),
        //       title: new Text('Home'),
        //     ),
        //     BottomNavigationBarItem(
        //       icon: IconButton(
        //         icon: Image.asset('assets/send.png'),
        //         iconSize: 40,
        //         onPressed: () {},
        //       ),
        //       title: new Text('Story'),
        //     ),
        //     BottomNavigationBarItem(
        //         icon: Icon(Icons.person), title: Text('Profile'))
        //   ],
        // ),
      );
  }

  titlePage() {
    return Padding(
      padding: const EdgeInsets.only(top: 5, left: 15),
      child: Row(
        children: [
          Text('Kopdar',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0)),
        ],
      ),
    );
  }

  List<String> menuKategori = [
    "assets/member.png",
    "assets/rss.png",
    "assets/twocar.png",
    "assets/adduser.png",
    "assets/shop.png",
    "assets/chat.png",
  ];
  List<String> menuName = [
    "Kopdar Gabungan",
    "Kopdar Santai",
    "Kopdar",
    "Daftar",
    "Marketplace",
    "Grup Chat",
  ];

  List imageSlider = ["assets/"];

  final List<KopdarModel> _kopdarlist = [
    KopdarModel(1, "Kopdar Gabungan", "Chapter: Bukit Timggi",
        "Kopdar gabungan HAI,\nLokasi: Caffe Anu\nJam: 20.00\nDi tunggu Kehadirannya"),
    KopdarModel(2, "Kopdar Santai", "Chapter: Jabotabek",
        "Kopdar gabungan HAI,\nLokasi: Caffe Anu\nJam: 20.00\nDi tunggu Kehadirannya"),
    KopdarModel(2, "Kopdar Keluarga", "Chapter: Jabotabek",
        "Kopdar gabungan HAI,\nLokasi: Caffe Anu\nJam: 20.00\nDi tunggu Kehadirannya"),
  ];

  final titles = ["Kopdar Gabungan", "Kopdar Santai", "Kopdar Keluarga"];
  final subtitles = [
    "Chapter: Bukit Timggi",
    "Chapter: Jabotabek",
    "Chapter: Jawa Barat"
  ];
  final isi = [
    "Kopdar gabungan HAI,\nLokasi: Caffe Anu\nJam: 20.00\nDi tunggu Kehadirannya",
    "Kopdar gabungan HAI,\nLokasi: Caffe Anu\nJam: 20.00\nDi tunggu Kehadirannya",
    "Kopdar gabungan HAI,\nLokasi: Caffe Anu\nJam: 20.00\nDi tunggu Kehadirannya"
  ];
}
