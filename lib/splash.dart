import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mobile/login.dart';
import 'package:mobile/views/dashboard.dart';
import 'package:mobile/main.dart';
import 'package:shared_preferences/shared_preferences.dart';

// void main() {
//   runApp(new MaterialApp(
//     home: new SplashScreen(),
//     routes: <String, WidgetBuilder>{
//       '/HomeScreen': (BuildContext context) => new Dashboard()
//     },
//   ));
// }
class SplashScreen extends StatefulWidget {
  final Color backgroundColor = Colors.white;
  final TextStyle styleTextUnderTheLoader = TextStyle(
      fontSize: 18.0, fontWeight: FontWeight.bold, color: Colors.black);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  String _versionName = 'V2.0.0';
  final splashDelay = 4;
  SharedPreferences prefs;
  var _currentUser;
  var _currIds;

  // Future<Null> getSharedPrefs() async {
  //   prefs = await SharedPreferences.getInstance();
  //   _currentUser = prefs.getString("currentUser");
  //   _currIds = prefs.getString("uid");
  //   if (_currentUser == null) {
  //     Navigator.pushReplacement(context,
  //         MaterialPageRoute(builder: (BuildContext context) => Login()));
  //     print('Harusnya Login');
  //   } else {
  //     _loadWidget();
  //   }
  // }

  @override
  void initState() {
    _loadWidget();
    super.initState();
    // Future.delayed(const Duration(seconds: 4), () {
    //   getSharedPrefs();
    // });
  }

  _loadWidget() async {
    var _duration = Duration(seconds: splashDelay);
    return Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => Dashboard()),
      (Route<dynamic> route) => false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: InkWell(
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 7,
                  child: Container(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        'assets/logosplash.png',
                        height: 120,
                        width: 120,
                        scale: 1.0,
                      ),
                      Text(
                        'Hyundai Accent Indonesia',
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                      ),
                    ],
                  )),
                ),
                Expanded(
                  child: Column(
                    children: <Widget>[
                      CircularProgressIndicator(),
                      Container(
                        height: 5.0,
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Spacer(),
                            Text(_versionName),
                            Spacer(
                              flex: 4,
                            ),
                            Text('loading...'),
                            Spacer(),
                          ])
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
