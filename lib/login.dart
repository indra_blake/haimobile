import 'dart:async';

import 'package:easy_loader/easy_loader.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:mobile/daftar.dart';
import 'package:mobile/utils/fadeanimation.dart';
import 'package:mobile/views/daftar.dart';
import 'package:mobile/views/dashboard.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _firebaseAuth = FirebaseAuth.instance;
  SharedPreferences prefs;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  var getEmail;
  bool isLogin = false;
  bool _isHidePassword = true;
  bool isLoginrun = false;
  bool emailNotValid = false;

  /* function login */
  // ignore: missing_return
  Future<User> signInEmailAndPassword(String email, String pass) async {
    //showMyCustomLoadingDialog(context);
    //context.loaderOverlay.show();
    User user;
    print('Jalanin signIN');
    try {
      UserCredential result = await _firebaseAuth.signInWithEmailAndPassword(
          email: email, password: pass);
      print('Result: $result');
      user = result.user;
      if (user.emailVerified) {
        String getUser = user.displayName ?? '';
        String em = user.email;
        String owner = user.uid;
        setIntoSharedPreferences(getUser, owner, em);
        print('uid email and Password: ${user.uid}');
        print('user: $em');
        isLogin = true;
        // setRegisterState();
        // setState(() {
        //   isRegister = true;
        //   print('isRegister: $isRegister');
        // });
        //runChangeButtonRelay();
        //context.loaderOverlay.hide();
        /* Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Dashboard(
          currentUSername: getUser,
          owner: owner,
          loginState: isLogin,
              )),(Route<dynamic> route) => false, */

        //Navigator.pushNamed(context, '/dashboard');
        //Navigator.of(context).pushNamed('/dashboard');

        Future.delayed(const Duration(milliseconds: 500), () {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (context) => Dashboard(
                      currentUser: getUser,
                      owner: owner,
                      loginState: isLogin,
                    )),
            (Route<dynamic> route) => false,
          );
          // Navigator.push(
          //     context,
          //     MaterialPageRoute(
          //         builder: (BuildContext context) => Dashboard(
          //               currentUser: getUser,
          //               owner: owner,
          //               loginState: isLogin,
          //             )));
        });
        isLoginrun = false;
      } else {
        setState(() {
          isLoginrun = false;
        });
        Toast.show('Akun anda belum terverifikasi, \n Silahkan Cek Email anda',
            context,
            duration: 2, gravity: Toast.CENTER);
        //runChangeButtonRelay();
        //context.loaderOverlay.hide();
      }
      //runChangeButtonRelay();
    } on FirebaseAuthException catch (e) {
      if (e.code == 'wrong-password') {
        setState(() {
          isLoginrun = false;
        });
        Toast.show('Password Salah', context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
        //runChangeButtonRelay();
        //context.loaderOverlay.hide();
      } else if (e.code == "invalid-email") {
        setState(() {
          emailNotValid = true;
          isLoginrun = false;
        });
        // Toast.show('Alamat email tidak valid', context,
        //     duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      } else if (e.code == "user-not-found") {
        //runChangeButtonRelay();
        //context.loaderOverlay.hide();
        Toast.show('User tidak ditemukan!!Anda belum Verifikasi', context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
        setState(() {
          isLoginrun = false;
        });
        //Timer(backTo, () => showAlertToRegister(context));
      } else if (e.code == 'too-many-requests') {
        setState(() {
          isLoginrun = false;
        });
        Toast.show('Anda sudah mencoba berkali-kali, coba lagi nanti', context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      }
      print(e);
    } catch (e) {
      print(e);
    }
  }

  void setIntoSharedPreferences(String name, String token, String email) async {
    prefs = await SharedPreferences.getInstance();
    bool isLoginState = true;
    await prefs.setString("currentUser", name);
    await prefs.setString('email', email);
    await prefs.setString("uid", token);
    await prefs.setBool("isLogin", isLoginState);
    print('Login Data :' + prefs.toString());
  }

  /* function show password */
  void _toggle() {
    setState(() {
      _isHidePassword = !_isHidePassword;
    });
  }

  /* function send email verif */
  Future<void> sendEmailVerification() async {
    final User user = _firebaseAuth.currentUser;
    user.sendEmailVerification();
  }

  @override
  void initState() {
    super.initState();
  }

  void configLoading() {}

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 400,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('assets/bg.png'),
                              fit: BoxFit.fill)),
                      child: Stack(
                        children: <Widget>[
                          Positioned(
                            left: 140,
                            width: 80,
                            height: 150,
                            child: FadeAnimation(
                                1.3,
                                Container(
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage(
                                              'assets/logosplash.png'))),
                                )),
                          ),

                          // Positioned(
                          //   right: 40,
                          //   top: 40,
                          //   width: 80,
                          //   height: 150,
                          //   child: FadeAnimation(1.5, Container(
                          //     decoration: BoxDecoration(
                          //       image: DecorationImage(
                          //         image: AssetImage('assets/logosplash.png')
                          //       )
                          //     ),
                          //   )),
                          // ),
                          Positioned(
                            child: FadeAnimation(
                                1.6,
                                Container(
                                  margin: EdgeInsets.only(bottom: 30),
                                  child: Center(
                                    child: Text(
                                      "Login",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 30,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                )),
                          ),
                          Positioned(
                            child: FadeAnimation(
                                1.6,
                                Container(
                                  margin: EdgeInsets.only(top: 60),
                                  child: Center(
                                    child: Text(
                                      "HAIMobile",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 40,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                )),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(30.0),
                      child: Column(
                        children: <Widget>[
                          FadeAnimation(
                              1.8,
                              Container(
                                padding: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color.fromRGBO(0, 11, 31, 183),
                                          blurRadius: 20.0,
                                          offset: Offset(0, 10))
                                    ]),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.all(8.0),
                                      decoration: BoxDecoration(
                                          border: Border(
                                              bottom: BorderSide(
                                                  color: Colors.grey[100]))),
                                      child: TextField(
                                        controller: emailController,
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        decoration: InputDecoration(
                                            border: InputBorder.none,
                                            hintText: "Masukan Email",
                                            hintStyle: TextStyle(
                                                color: Colors.grey[400])),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(8.0),
                                      child: TextField(
                                        controller: passwordController,
                                        obscureText: _isHidePassword,
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: "Kata Sandi",
                                          hintStyle: TextStyle(
                                              color: Colors.grey[400]),
                                          suffixIcon: GestureDetector(
                                            onTap: () {
                                              _toggle();
                                            },
                                            child: Icon(
                                              _isHidePassword
                                                  ? Icons.visibility_off
                                                  : Icons.visibility,
                                              color: _isHidePassword
                                                  ? Colors.grey
                                                  : Colors.blue,
                                              size: 20.0,
                                            ),
                                          ),
                                          isDense: true,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              )),
                          SizedBox(
                            height: 30,
                          ),
                          FadeAnimation(
                              2,
                              GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  onTap: () {
                                    if (emailController.text.isEmpty) {
                                      Toast.show(
                                          'Email tidak boleh kosong', context,
                                          duration: Toast.LENGTH_LONG,
                                          backgroundColor: Colors.black);
                                      // SmartDialog.showToast(
                                      //     'Email tidak boleh kosong');
                                    } else if (passwordController
                                        .text.isEmpty) {
                                      Toast.show('Password tidak boleh kosong',
                                          context,
                                          duration: Toast.LENGTH_LONG,
                                          backgroundColor: Colors.black);
                                      // SmartDialog.showToast(
                                      //     'Password tidak boleh kosong');
                                    } else if (passwordController.text.length <
                                        5) {
                                      Toast.show(
                                          'Password minimal 8 digit', context,
                                          duration: Toast.LENGTH_LONG,
                                          backgroundColor: Colors.black);
                                      // SmartDialog.showToast(
                                      //     'Password minimal 8 digit');
                                    } else if (emailController.text.length <
                                        8) {
                                      Toast.show('Email tidak valid', context,
                                          duration: Toast.LENGTH_LONG,
                                          backgroundColor: Colors.black);
                                      // SmartDialog.showToast('Email tidak valid');
                                    } else {
                                      setState(() {
                                        signInEmailAndPassword(
                                            emailController.text,
                                            passwordController.text);
                                        isLoginrun = true;
                                        getEmail = emailController.text;
                                        //_showLoading();
                                      });
                                    }
                                    print('Login');
                                  },
                                  child: Container(
                                    height: 50,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        gradient: LinearGradient(colors: [
                                          Colors.blue[800],
                                          Colors.blueAccent[700],
                                        ])),
                                    child: Center(
                                      child: Text(
                                        "Masuk",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ))),
                          SizedBox(
                            height: 10,
                          ),
                          FadeAnimation(
                              1.5,
                              Text(
                                "Belum punya akun?",
                                style: TextStyle(color: Colors.blue[800]),
                              )),
                          FadeAnimation(
                            1.5,
                            Builder(
                                builder: (context) => TextButton(
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  DaftarPage()));
                                    },
                                    child: Text(
                                      "Daftar Sekarang!",
                                      style: TextStyle(color: Colors.blue[800]),
                                    ))),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            isLoginrun
                ? EasyLoader(
                    image: AssetImage(
                      'assets/logosplash.png',
                    ),
                    iconColor: Colors.white,
                  )
                : Container()
          ],
        ));
  }

  openDaftar() {
    Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) => DaftarPage()));
    print('KeDaftar');
  }

  void _showLoading() async {}
}
