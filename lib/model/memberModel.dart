class MemberModel {
  final String id;
  final String name;
  final String avatar;
  final String jabatan;
  final String nopung;
  final String chapter;
  final String unit;
  final String joinDate;
  MemberModel(this.id, this.name, this.avatar, this.jabatan, this.nopung,
      this.chapter, this.unit, this.joinDate);

  MemberModel.fromJson(Map<String, dynamic> json)
      : id = json['id_member'],
        name = json['nama'],
        avatar = json['avatar'],
        jabatan = json['status_member'],
        nopung = json['nopung'],
        chapter = json['domisili'],
        unit = json['tipe_mobil'],
        joinDate = json['reg_date'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'nama': name,
        'avatar': avatar,
        'status': jabatan,
        'nopung': nopung,
        'domisili': chapter,
        'tipe_mobil': unit,
        'content': joinDate,
        'reg_date': name,
      };
}
