class ArtikelModel {
  final int id;
  final String title;
  final String author;
  final String description;
  final String url;
  final String urlToImage;
  final String publishedAt;
  final String content;
  final String name;

  ArtikelModel(this.id, this.author, this.description, this.url, this.urlToImage, this.publishedAt, this.content, this.name, this.title);
  
  ArtikelModel.fromJson(Map<String, dynamic> json)
    : id = json['id'],
    title = json['title'],
    author = json['author'],
    description = json['description'],
    url = json['url'],
    urlToImage = json['urlImage'],
    publishedAt = json['publishedAt'],
    content = json['content'],
    name = json['name'];

  Map<String, dynamic> toJson() => {
    'id':id,
    'title':title,
    'author':author,
    'description':description,
    'url':url,
    'urlImage':urlToImage,
    'publishedAt':publishedAt,
    'content':content,
    'name':name,
  };  

}