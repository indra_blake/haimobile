class StoryModel {
  final int id;
  final String name;
  final String avatar;
  final String statusTitle;
  final String imagePic;
  final String like;
  final String comment;
  final String postDate;
  final String commentUID;
  StoryModel(this.id, this.name, this.avatar,this.statusTitle,this.imagePic,this.postDate, this.like, this.comment, this.commentUID);

  StoryModel.fromJson(Map<String, dynamic> json)
      : id = json['owner'],
        name = json['namaposter'],
        avatar = json['avatar'],
        statusTitle = json['title'],
        imagePic = json['imageurl'],
        like = json['isLike'],
        comment = json['isComment'],
        postDate = json['timepost'],
        commentUID = json['commentUID'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'nama': name,
        'avatar': avatar,
        'title': statusTitle,
        'image': imagePic,
        'like': like,
        'comment': comment,
        'timepost': postDate,
        'commentuid': commentUID,
       
      };
}