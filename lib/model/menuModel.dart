class MenuModel {
  final int id;
  final String menuTitle;
  final String menuIcon;

  MenuModel(this.id, this.menuTitle, this.menuIcon);

  MenuModel.fromJson(Map<String, dynamic> json)
    : id = json['id'],
    menuTitle = json['menuTitle'],
    menuIcon = json['menuIcon'];

  Map<String, dynamic> toJson() => {
    'id':id,
    'menuTitle':menuTitle,
    'menuIcon':menuIcon,
  };  
    
}